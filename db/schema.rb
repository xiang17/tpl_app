# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20151119035827) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "active_admin_comments", force: :cascade do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "admin_users", force: :cascade do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "appointments", force: :cascade do |t|
    t.integer  "appointment_type", default: 0,  null: false
    t.string   "address",          default: "", null: false
    t.string   "city",             default: "", null: false
    t.string   "state",            default: "", null: false
    t.string   "zipcode",          default: "", null: false
    t.string   "car_address",      default: ""
    t.string   "car_city",         default: ""
    t.string   "car_state",        default: ""
    t.string   "car_zipcode",      default: ""
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
    t.integer  "user_id"
    t.integer  "expert_id"
    t.integer  "time_window_id"
    t.integer  "car_id"
    t.integer  "time_window_ids",                            array: true
    t.integer  "sell_request_id"
  end

  add_index "appointments", ["car_id"], name: "index_appointments_on_car_id", using: :btree
  add_index "appointments", ["expert_id"], name: "index_appointments_on_expert_id", using: :btree
  add_index "appointments", ["sell_request_id"], name: "index_appointments_on_sell_request_id", using: :btree
  add_index "appointments", ["time_window_id"], name: "index_appointments_on_time_window_id", using: :btree
  add_index "appointments", ["user_id"], name: "index_appointments_on_user_id", using: :btree

  create_table "car_colors", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "car_colors", ["name"], name: "index_car_colors_on_name", using: :btree

  create_table "car_feature_relations", force: :cascade do |t|
    t.integer  "car_id"
    t.integer  "feature_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "car_feature_relations", ["car_id"], name: "index_car_feature_relations_on_car_id", using: :btree
  add_index "car_feature_relations", ["feature_id"], name: "index_car_feature_relations_on_feature_id", using: :btree

  create_table "car_features", force: :cascade do |t|
    t.string   "name"
    t.string   "value"
    t.float    "msrp"
    t.string   "icon_path"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "car_features", ["name"], name: "index_car_features_on_name", using: :btree

  create_table "car_model_details", force: :cascade do |t|
    t.integer  "styleId"
    t.string   "exteriorcolors",                    array: true
    t.string   "interiorcolors",                    array: true
    t.string   "roofcolors",                        array: true
    t.string   "fuel",                              array: true
    t.string   "drivetrain",                        array: true
    t.string   "engineperformance",                 array: true
    t.string   "exteriormeasurements",              array: true
    t.string   "interiormeasurements",              array: true
    t.string   "weightsandcapacities",              array: true
    t.string   "suspension",                        array: true
    t.string   "warranty",                          array: true
    t.string   "frontseats",                        array: true
    t.string   "rearseats",                         array: true
    t.string   "powerfeatures",                     array: true
    t.string   "instrumentation",                   array: true
    t.string   "convenience",                       array: true
    t.string   "comfort",                           array: true
    t.string   "memorized",                         array: true
    t.string   "audio",                             array: true
    t.string   "telematics",                        array: true
    t.string   "roofandglass",                      array: true
    t.string   "tiresandwheels",                    array: true
    t.string   "safetyfeatures",                    array: true
    t.string   "truckfeatures",                     array: true
    t.string   "towingandhauling",                  array: true
    t.string   "doors",                             array: true
    t.string   "body",                              array: true
    t.string   "package",                           array: true
    t.string   "exterior",                          array: true
    t.string   "interior",                          array: true
    t.string   "mechanical",                        array: true
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "car_model_id"
  end

  add_index "car_model_details", ["car_model_id"], name: "index_car_model_details_on_car_model_id", using: :btree

  create_table "car_models", force: :cascade do |t|
    t.string   "make"
    t.string   "make_niceName"
    t.string   "model"
    t.string   "modelId"
    t.string   "model_niceName"
    t.integer  "year"
    t.string   "body"
    t.string   "fuel"
    t.string   "trim"
    t.string   "modelName"
    t.string   "styleName"
    t.integer  "styleId"
    t.string   "styleURL"
    t.string   "fuelEconomy"
    t.string   "transmission"
    t.string   "basicWarranty"
    t.string   "bluetooth"
    t.string   "heatedSeats"
    t.string   "engineType"
    t.string   "totalSeating"
    t.string   "cylinders"
    t.string   "driveTrain"
    t.string   "navigation"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

  create_table "car_pictures", force: :cascade do |t|
    t.integer  "car_id"
    t.string   "picture"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
    t.integer  "picture_type"
    t.string   "description"
  end

  add_index "car_pictures", ["car_id"], name: "index_car_pictures_on_car_id", using: :btree

  create_table "car_transactions", force: :cascade do |t|
    t.integer  "transaction_id"
    t.float    "total_amount"
    t.integer  "buyer_id"
    t.integer  "seller_id"
    t.integer  "car_id"
    t.integer  "expert_id"
    t.integer  "status"
    t.string   "name"
    t.string   "email"
    t.string   "phone"
    t.integer  "account_type"
    t.string   "routing_number"
    t.string   "account_number"
    t.string   "billing_address",  default: ""
    t.string   "billing_city",     default: ""
    t.string   "billing_state",    default: ""
    t.string   "billing_zipcode",  default: ""
    t.string   "delivery_address", default: ""
    t.string   "delivery_city",    default: ""
    t.string   "delivery_state",   default: ""
    t.string   "delivery_zipcode", default: ""
    t.datetime "created_at",                    null: false
    t.datetime "updated_at",                    null: false
  end

  add_index "car_transactions", ["status"], name: "index_car_transactions_on_status", using: :btree
  add_index "car_transactions", ["transaction_id"], name: "index_car_transactions_on_transaction_id", using: :btree

  create_table "cars", force: :cascade do |t|
    t.integer  "mileage"
    t.integer  "owner_id"
    t.integer  "inspector_id"
    t.integer  "car_model_id"
    t.string   "int_color"
    t.string   "ext_color"
    t.integer  "price_edmunds_tradein"
    t.integer  "price_dealer_tradein"
    t.integer  "price"
    t.integer  "price_lower_bound"
    t.integer  "price_upper_bound"
    t.integer  "price_guarantee"
    t.integer  "price_edmunds_retail"
    t.integer  "price_dealer_retail"
    t.integer  "price_kbb_suggest_retail"
    t.integer  "price_kbb_max_retail"
    t.integer  "outstanding_price"
    t.integer  "clean_price"
    t.integer  "average_price"
    t.integer  "trade_in_rough_price"
    t.integer  "price_registration_fee",   default: 42
    t.integer  "price_document_fee",       default: 50
    t.integer  "price_service_fee"
    t.string   "vin"
    t.float    "condition"
    t.integer  "full_warranty",            default: 0
    t.integer  "drivetrain_warranty",      default: 0
    t.string   "address"
    t.string   "city"
    t.string   "state"
    t.string   "zipcode"
    t.boolean  "sold",                     default: false
    t.string   "carfax"
    t.string   "autocheck"
    t.string   "checklistpdf"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
    t.integer  "interior_color_id"
    t.integer  "exterior_color_id"
    t.integer  "status"
    t.boolean  "ABS_4_wheel",              default: false
    t.boolean  "keyless_entry",            default: false
    t.boolean  "navigation_system",        default: false
    t.boolean  "bluetooth_wireless",       default: false
    t.boolean  "parking_sensors",          default: false
    t.boolean  "backup_camera",            default: false
    t.boolean  "heated_seats",             default: false
    t.boolean  "leather",                  default: false
    t.boolean  "panorama_roof",            default: false
    t.boolean  "sunroof",                  default: false
    t.boolean  "moonroof",                 default: false
    t.boolean  "usb",                      default: false
    t.boolean  "premium_wheels_19_plus",   default: false
    t.boolean  "premium_wheels",           default: false
    t.boolean  "premium_sound",            default: false
    t.boolean  "steering_wheel_controls",  default: false
    t.boolean  "premium_lights",           default: false
    t.boolean  "head_up_display",          default: false
    t.boolean  "seat_memory",              default: false
    t.string   "checklist",                                             array: true
    t.integer  "soldinndays",              default: -1
    t.string   "thumbnail"
    t.string   "detail"
    t.string   "love"
  end

  add_index "cars", ["car_model_id"], name: "index_cars_on_car_model_id", using: :btree
  add_index "cars", ["inspector_id"], name: "index_cars_on_inspector_id", using: :btree
  add_index "cars", ["mileage"], name: "index_cars_on_mileage", using: :btree
  add_index "cars", ["owner_id"], name: "index_cars_on_owner_id", using: :btree

  create_table "charges", force: :cascade do |t|
    t.integer  "userid",     default: 0,  null: false
    t.integer  "carid",      default: 0,  null: false
    t.string   "email",      default: "", null: false
    t.string   "phone",      default: "", null: false
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
    t.string   "price"
    t.integer  "year"
    t.string   "make"
    t.string   "model"
  end

  create_table "easy_transfers", force: :cascade do |t|
    t.string   "source",              default: "tripalink"
    t.string   "serial_number"
    t.string   "submission_time"
    t.string   "college_code",        default: "V00001"
    t.float    "tuition"
    t.float    "tuition_rmb"
    t.string   "payment_types"
    t.string   "name_en"
    t.string   "name_ch"
    t.string   "student_number"
    t.string   "student_id_number"
    t.string   "actual_payer_name"
    t.string   "actual_payer_email"
    t.string   "actual_payer_mobile"
    t.string   "hmac_code"
    t.string   "promo_code"
    t.string   "attachment"
    t.integer  "user_id"
    t.integer  "car_id"
    t.integer  "result1"
    t.string   "message1"
    t.integer  "result2"
    t.string   "message2"
    t.datetime "created_at",                                null: false
    t.datetime "updated_at",                                null: false
  end

  add_index "easy_transfers", ["car_id"], name: "index_easy_transfers_on_car_id", using: :btree
  add_index "easy_transfers", ["user_id"], name: "index_easy_transfers_on_user_id", using: :btree

  create_table "experts", force: :cascade do |t|
    t.string   "name",                   default: "",  null: false
    t.string   "phone",                  default: "",  null: false
    t.integer  "experience",             default: 0,   null: false
    t.string   "address",                default: "",  null: false
    t.string   "city",                   default: "",  null: false
    t.string   "state",                  default: "",  null: false
    t.string   "zipcode",                default: "",  null: false
    t.string   "organization",           default: "",  null: false
    t.string   "biograph",               default: "",  null: false
    t.float    "rating",                 default: 0.0, null: false
    t.string   "avatar",                 default: "",  null: false
    t.string   "work_zipcode",           default: "",  null: false
    t.integer  "work_radius",            default: 20,  null: false
    t.float    "lat"
    t.float    "lng"
    t.string   "jwt_token",              default: ""
    t.string   "email",                  default: "",  null: false
    t.string   "encrypted_password",     default: "",  null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,   null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
  end

  add_index "experts", ["email"], name: "index_experts_on_email", unique: true, using: :btree
  add_index "experts", ["reset_password_token"], name: "index_experts_on_reset_password_token", unique: true, using: :btree

  create_table "sell_requests", force: :cascade do |t|
    t.integer  "mileage",              null: false
    t.string   "vin"
    t.datetime "created_at",           null: false
    t.datetime "updated_at",           null: false
    t.integer  "user_id"
    t.integer  "car_model_id"
    t.integer  "outstanding_price"
    t.integer  "clean_price"
    t.integer  "average_price"
    t.integer  "trade_in_rough_price"
  end

  add_index "sell_requests", ["car_model_id"], name: "index_sell_requests_on_car_model_id", using: :btree
  add_index "sell_requests", ["user_id"], name: "index_sell_requests_on_user_id", using: :btree

  create_table "time_windows", force: :cascade do |t|
    t.date     "date",                   null: false
    t.integer  "hour",                   null: false
    t.integer  "status",     default: 0, null: false
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
    t.integer  "expert_id",              null: false
  end

  add_index "time_windows", ["expert_id"], name: "index_time_windows_on_expert_id", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "name",                   default: "",    null: false
    t.string   "phone",                  default: "",    null: false
    t.boolean  "admin",                  default: false, null: false
    t.string   "avatar",                 default: "",    null: false
    t.string   "address",                default: "",    null: false
    t.string   "city",                   default: "",    null: false
    t.string   "state",                  default: "",    null: false
    t.string   "zipcode",                default: "",    null: false
    t.float    "lat"
    t.float    "lng"
    t.string   "provider"
    t.string   "uid"
    t.string   "jwt_token",              default: ""
    t.string   "email",                  default: "",    null: false
    t.string   "encrypted_password",     default: "",    null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,     null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree

  add_foreign_key "appointments", "cars"
  add_foreign_key "appointments", "experts"
  add_foreign_key "appointments", "sell_requests"
  add_foreign_key "appointments", "time_windows"
  add_foreign_key "appointments", "users"
  add_foreign_key "car_model_details", "car_models", on_delete: :cascade
  add_foreign_key "car_pictures", "cars", on_delete: :cascade
  add_foreign_key "cars", "car_models", on_delete: :restrict
  add_foreign_key "easy_transfers", "cars", on_delete: :restrict
  add_foreign_key "easy_transfers", "users", on_delete: :restrict
  add_foreign_key "sell_requests", "car_models"
  add_foreign_key "sell_requests", "users"
  add_foreign_key "time_windows", "experts", on_delete: :cascade
end
