class ModifySellRequest < ActiveRecord::Migration
  def change
    add_column :sell_requests, :outstanding_price, :integer
    add_column :sell_requests, :clean_price, :integer
    add_column :sell_requests, :average_price, :integer
    add_column :sell_requests, :trade_in_rough_price, :integer
  end
end
