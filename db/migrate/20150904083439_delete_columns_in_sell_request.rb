class DeleteColumnsInSellRequest < ActiveRecord::Migration
  def change
    remove_column :sell_requests, :year
    remove_column :sell_requests, :make
    remove_column :sell_requests, :model
    remove_column :sell_requests, :body_style
    remove_column :sell_requests, :trim
    remove_column :sell_requests, :license_plate
    remove_column :sell_requests, :plate_state
    remove_column :sell_requests, :other_info

  end
end
