class AddCheckListToCars < ActiveRecord::Migration
  def change
    add_column  :cars, :checklist, :string, :array => true
  end
end
