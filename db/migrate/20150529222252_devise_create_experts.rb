class DeviseCreateExperts < ActiveRecord::Migration
  def change
    create_table(:experts) do |t|
      t.string  :name,         null: false, default: ""
      t.string  :phone,        null: false, default: ""
      t.integer :experience,   null: false, default: 0
      t.string  :address,      null: false, default: ""
      t.string  :city,         null: false, default: ""
      t.string  :state,        null: false, default: ""
      t.string  :zipcode,      null: false, default: ""
      t.string  :organization, null: false, default: ""
      t.string  :biograph,     null: false, default: ""
      t.float   :rating,       null: false, default: 0
      t.string  :avatar,       null: false, default: ""

      t.string  :work_zipcode, null: false, default: ""
      t.integer :work_radius,  null: false, default: 20

      t.float :lat
      t.float :lng
      
      ##JWT authentication
      t.string :jwt_token, default: ""

      ## Database authenticatable
      t.string :email,              null: false, default: ""
      t.string :encrypted_password, null: false, default: ""

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, default: 0, null: false
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.inet     :current_sign_in_ip
      t.inet     :last_sign_in_ip

      ## Confirmable
      #t.string   :confirmation_token
      #t.datetime :confirmed_at
      #t.datetime :confirmation_sent_at
      #t.string   :unconfirmed_email # Only if using reconfirmable

      ## Lockable
      # t.integer  :failed_attempts, default: 0, null: false # Only if lock strategy is :failed_attempts
      # t.string   :unlock_token # Only if unlock strategy is :email or :both
      # t.datetime :locked_at

      t.timestamps null: false
    end

    add_index :experts, :email,                unique: true
    add_index :experts, :reset_password_token, unique: true
    #add_index :experts, :confirmation_token,   unique: true
    # add_index :experts, :unlock_token,         unique: true
  end
end
