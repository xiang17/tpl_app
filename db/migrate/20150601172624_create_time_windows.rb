class CreateTimeWindows < ActiveRecord::Migration
  def change
    create_table :time_windows do |t|
      t.date    :date,   null: false
      t.integer :hour,   null: false
      
      #status: 0=not available; 1=available; 2=appointment
      t.integer :status, null: false, default: 0

      t.timestamps null: false
    end
    
    add_reference :time_windows, :expert, index: true, null: false
    add_foreign_key :time_windows, :experts, on_delete: :cascade
  end
end
