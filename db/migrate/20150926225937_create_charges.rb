class CreateCharges < ActiveRecord::Migration
  def change
    create_table :charges do |t|
      t.integer :userid, null: false, default: 0
      t.integer :carid, null: false, default: 0
      t.string :email, null: false, default: ""
      t.string :phone, null: false, default: ""
      t.timestamps null: false
    end
  end
end
