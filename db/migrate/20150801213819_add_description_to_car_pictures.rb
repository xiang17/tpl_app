class AddDescriptionToCarPictures < ActiveRecord::Migration
  def change
    add_column  :car_pictures, :description, :string
  end
end
