class CreateEasyTransfers < ActiveRecord::Migration
  def change
    create_table :easy_transfers do |t|
      t.string :source, default: 'tripalink'
      t.string :serial_number
      t.string :submission_time
      t.string :college_code, default: 'V00001'
      t.float :tuition
      t.float :tuition_rmb
      t.string :payment_types
      t.string :name_en
      t.string :name_ch
      t.string :student_number
      t.string :student_id_number
      t.string :actual_payer_name
      t.string :actual_payer_email
      t.string :actual_payer_mobile
      t.string :hmac_code
      t.string :promo_code
      t.string :attachment

      t.references :user, index: true
      t.references :car, index: true

      t.integer :result1
      t.string :message1
      t.integer :result2
      t.string :message2

      t.timestamps null: false
    end

    add_foreign_key :easy_transfers, :users, on_delete: :restrict
    add_foreign_key :easy_transfers, :cars, on_delete: :restrict
  end
end
