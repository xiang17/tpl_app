class CreateCarColors < ActiveRecord::Migration
  def change
    create_table :car_colors do |t|
      t.string :name, index: true
      t.string :value

      t.timestamps null: false
    end
  end
end
