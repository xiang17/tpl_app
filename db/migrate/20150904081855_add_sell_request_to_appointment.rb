class AddSellRequestToAppointment < ActiveRecord::Migration
  def change
    add_reference :appointments, :sell_request, index: true
    add_foreign_key :appointments, :sell_requests, index: true
  end
end
