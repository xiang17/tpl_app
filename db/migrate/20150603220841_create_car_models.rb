class CreateCarModels < ActiveRecord::Migration
  def change
    create_table :car_models do |t|
      t.string :make
      t.string :make_niceName
      t.string :model
      t.string :modelId
      t.string :model_niceName
      t.integer :year
      t.string :body
      t.string :fuel
      t.string :trim
      t.string :modelName
      t.string :styleName
      t.integer :styleId
      t.string :styleURL
      t.string :fuelEconomy
      t.string :transmission
      t.string :basicWarranty
      t.string :bluetooth
      t.string :heatedSeats
      t.string :engineType
      t.string :totalSeating
      t.string :cylinders
      t.string :driveTrain
      t.string :navigation

      t.timestamps null: false
    end
  end
end
