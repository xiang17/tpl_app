class CreateCarTransactions < ActiveRecord::Migration
  def change
    create_table :car_transactions do |t|
      t.integer :transaction_id, index: true
      t.float :total_amount
      t.references :buyer
      t.references :seller
      t.references :car
      t.references :expert
      t.integer :status, index: true

      t.string :name
      t.string :email
      t.string :phone

      t.integer :account_type
      t.string :routing_number
      t.string :account_number

      t.string :billing_address, default: ""
      t.string :billing_city, default: ""
      t.string :billing_state, default: ""
      t.string :billing_zipcode, default: ""

      t.string :delivery_address, default: ""
      t.string :delivery_city, default: ""
      t.string :delivery_state, default: ""
      t.string :delivery_zipcode, default: ""

      t.timestamps null: false
    end
  end
end
