class AddForeignKeyToAppointment < ActiveRecord::Migration
  def change
    add_reference   :appointments, :car,  index: true
    add_foreign_key :appointments, :cars, index: true
  end
end
