class CreateAppointments < ActiveRecord::Migration
  def change
    create_table :appointments do |t|
      #appointment type: 0=sell; 1=buy
      t.integer :appointment_type, null: false, default: 0
      t.string :address, null: false, default: ""
      t.string :city, null: false, default: ""
      t.string :state, null: false, default: ""
      t.string :zipcode, null: false, default: ""

      t.string :car_address,  default: ""
      t.string :car_city,  default: ""
      t.string :car_state,  default: ""
      t.string :car_zipcode, default: ""

      t.timestamps null: false
    end

    add_reference :appointments, :user,         index: true
    add_reference :appointments, :expert,       index: true
    add_reference :appointments, :time_window,  index: true

    add_foreign_key :appointments, :users,         index: true
    add_foreign_key :appointments, :experts,       index: true
    add_foreign_key :appointments, :time_windows,  index: true
  end
end
