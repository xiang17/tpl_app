class AddCarModelFieldToSellrequest < ActiveRecord::Migration
  def change
    add_reference :sell_requests, :car_model, index: true
    add_foreign_key :sell_requests, :car_models, index: true
  end
end
