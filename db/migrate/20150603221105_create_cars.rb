class CreateCars < ActiveRecord::Migration
  def change
    create_table :cars do |t|
      t.integer :mileage
      t.references :owner, index: true
      t.references :inspector, index: true
      t.references :car_model, index: true

      t.string :int_color
      t.string :ext_color

      t.integer :price_edmunds_tradein  #edmunds would buy at this price
      t.integer :price_dealer_tradein   #dealer would buy at this price
      t.integer :price
      t.integer :price_lower_bound      #recommended price range, lower bound
      t.integer :price_upper_bound      #recommended price range, upper bound
      t.integer :price_guarantee        #tripalink would buy at this price if not sold within time range

      t.integer :price_edmunds_retail  #edmunds would sell at this price
      t.integer :price_dealer_retail   #dealer would sell at this price

      t.integer :price_kbb_suggest_retail
      t.integer :price_kbb_max_retail

      #下面四个没用
      t.integer :outstanding_price
      t.integer :clean_price
      t.integer :average_price
      t.integer :trade_in_rough_price

      t.integer :price_registration_fee, default: 42
      t.integer :price_document_fee, default: 50
      t.integer :price_service_fee

      t.string :vin
      t.float :condition
      t.integer :full_warranty, default: 0
      t.integer :drivetrain_warranty, default: 0

      t.string :address, :city, :state, :zipcode
      t.boolean :sold, default: false

      t.string :carfax, :autocheck, :checklistpdf

      t.timestamps null: false

      # Reference for color
      t.references :interior_color
      t.references :exterior_color

      # Status indication
      t.integer :status
    end
    #add_foreign_key :cars, :users
    add_foreign_key :cars, :car_models, on_delete: :restrict
    
    # add_index :cars, :price
    add_index :cars, :mileage
  end
end
