class CreateCarPictures < ActiveRecord::Migration
  def change
    create_table :car_pictures do |t|
      t.references :car, index: true
      t.string :picture
      t.timestamps null: false
    end
    add_foreign_key :car_pictures, :cars, on_delete: :cascade
  end
end
