class CreateSellRequests < ActiveRecord::Migration
  def change
    create_table :sell_requests do |t|
      t.integer :year,          null:    false
      t.string  :make,          null:    false
      t.string  :model,         null:    false
      t.string  :body_style,    null:    false
      t.string  :trim,          default: ""
      t.integer :mileage,       null:    false

      t.string  :vin
      t.string  :license_plate
      t.string  :plate_state
      t.string  :other_info,    default: ""

      t.timestamps null: false
    end

    add_reference :sell_requests, :user, index: true
    add_foreign_key :sell_requests, :users
  end
end
