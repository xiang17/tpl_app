class CreateCarFeatureRelations < ActiveRecord::Migration
  def change
    create_table :car_feature_relations do |t|
      t.references :car, index: true
      t.references :feature, index: true
      t.timestamps null: false
    end
  end
end
