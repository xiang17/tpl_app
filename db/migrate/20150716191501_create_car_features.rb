class CreateCarFeatures < ActiveRecord::Migration
  def change
    create_table :car_features do |t|
      t.string :name, index: true
      t.string :value
      t.float :msrp
      t.string :icon_path

      t.timestamps null: false
    end
  end
end
