class CreateCarModelDetails < ActiveRecord::Migration
  def change
    create_table :car_model_details do |t|
      t.integer :styleId

      t.string :exteriorcolors, :array => true
      t.string :interiorcolors, :array => true
      t.string :roofcolors, :array => true
      t.string :fuel, :array => true
      t.string :drivetrain, :array => true
      t.string :engineperformance, :array => true
      t.string :exteriormeasurements, :array => true
      t.string :interiormeasurements, :array => true
      t.string :weightsandcapacities, :array => true
      t.string :suspension, :array => true
      t.string :warranty, :array => true

      t.string :frontseats, :array => true
      t.string :rearseats, :array => true
      t.string :powerfeatures, :array => true
      t.string :instrumentation, :array => true
      t.string :convenience, :array => true
      t.string :comfort, :array => true
      t.string :memorized, :array => true
      t.string :audio, :array => true
      t.string :telematics, :array => true

      t.string :roofandglass, :array => true
      t.string :tiresandwheels, :array => true
      t.string :safetyfeatures, :array => true
      t.string :truckfeatures, :array => true
      t.string :towingandhauling, :array => true
      t.string :doors, :array => true
      t.string :body, :array => true

      t.string :package, :array => true
      t.string :exterior, :array => true
      t.string :interior, :array => true
      t.string :mechanical, :array => true

      t.timestamps null: false
    end
  end
end
