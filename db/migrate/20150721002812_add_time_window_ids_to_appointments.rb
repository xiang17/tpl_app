class AddTimeWindowIdsToAppointments < ActiveRecord::Migration
  def change
    add_column  :appointments, :time_window_ids, :integer, :array => true
  end
end
