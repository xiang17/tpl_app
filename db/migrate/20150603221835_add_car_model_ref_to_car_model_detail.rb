class AddCarModelRefToCarModelDetail < ActiveRecord::Migration
  def change
    #add_reference :car_model_details, :car_model, index: true, foreign_key: true
    add_reference :car_model_details, :car_model, index: true
    add_foreign_key :car_model_details, :car_models, on_delete: :cascade
  end
end
