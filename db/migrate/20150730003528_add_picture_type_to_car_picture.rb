class AddPictureTypeToCarPicture < ActiveRecord::Migration
  def change
    add_column :car_pictures, :picture_type, :integer
  end
end
