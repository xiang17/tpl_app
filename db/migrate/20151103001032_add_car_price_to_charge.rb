class AddCarPriceToCharge < ActiveRecord::Migration
  def change
    add_column :charges, :price, :string
  end
end
