class AddOptionsToCars < ActiveRecord::Migration
  def change
    add_column  :cars, :ABS_4_wheel, :boolean, default: false
    add_column  :cars, :keyless_entry, :boolean, default: false
    add_column  :cars, :navigation_system, :boolean, default: false
    add_column  :cars, :bluetooth_wireless, :boolean, default: false
    add_column  :cars, :parking_sensors, :boolean, default: false
    add_column  :cars, :backup_camera, :boolean, default: false
    add_column  :cars, :heated_seats, :boolean, default: false
    add_column  :cars, :leather, :boolean, default: false
    add_column  :cars, :panorama_roof, :boolean, default: false
    add_column  :cars, :sunroof, :boolean, default: false
    add_column  :cars, :moonroof, :boolean, default: false
    add_column  :cars, :usb, :boolean, default: false
    add_column  :cars, :premium_wheels_19_plus, :boolean, default: false
    add_column  :cars, :premium_wheels , :boolean, default: false
    add_column  :cars, :premium_sound , :boolean, default: false
    add_column  :cars, :steering_wheel_controls, :boolean, default: false
    add_column  :cars, :premium_lights, :boolean, default: false
    add_column  :cars, :head_up_display, :boolean, default: false
    add_column  :cars, :seat_memory, :boolean, default: false
  end
end
