class AddThumbnailToCar < ActiveRecord::Migration
  def change
    add_column :cars, :thumbnail, :string
  end
end
