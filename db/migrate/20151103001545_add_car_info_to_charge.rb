class AddCarInfoToCharge < ActiveRecord::Migration
  def change
    add_column :charges, :year, :integer
    add_column :charges, :make, :string
    add_column :charges, :model, :string
  end
end
