CarColor.create!([
  {name: "black", value: "#000000"},
  {name: "silver", value: "#C0C0C0"},
  {name: "white", value: "#FFFFFF"},
  {name: "red", value: "#FF0000"},
  {name: "dark_red", value: "#8B0000"},
  {name: "orange", value: "#FFA500"},
  {name: "yellow", value: "#FFFF00"},
  {name: "gold", value: "#FFD700"},
  {name: "brown", value: "#8B4513"},
  {name: "green", value: "#2E8B57"},
  {name: "blue", value: "#0000CD"},
  {name: "purple", value: "#800080"},
  {name: "pink", value: "#FF69B4"},
  {name: "choclate", value: "#D2691E"},
  {name: "gray", value: "#808080"},
  {name: "silk", value: "#FFF8DC"},
  {name: "beige", value: "#F5F5DC"}
])

CarFeature.create!([
  {id: 1, name: 'ABS (4-Wheel)', value: nil, msrp: nil},
  {id: 2, name: 'Keyless Entry', value: nil, msrp: nil},
  {id: 3, name: 'Navigation System', value: nil, msrp: nil},
  {id: 4, name: 'Bluetooth Wireless', value: nil, msrp: nil},
  {id: 5, name: 'Parking Sensors', value: nil, msrp: nil},
  {id: 6, name: 'Backup Camera', value: nil, msrp: nil},
  {id: 7, name: 'Heated Seats', value: nil, msrp: nil},
  {id: 8, name: 'Leather', value: nil, msrp: nil},
  {id: 9, name: 'Panorama Roof', value: nil, msrp: nil},
  {id: 10, name: 'Sunroof', value: nil, msrp: nil},
  {id: 11, name: 'Moonroof', value: nil, msrp: nil},
  {id: 12, name: 'USB', value: nil, msrp: nil},
  {id: 13, name: 'Premium Wheels 19"+', value: nil, msrp: nil},
  {id: 14, name: 'Premium Wheels', value: nil, msrp: nil},
  {id: 15, name: 'Premium Sound', value: nil, msrp: nil},
  {id: 16, name: 'Steering Wheel Controls', value: nil, msrp: nil},
  {id: 17, name: 'Premium Lights', value: nil, msrp: nil},
  {id: 18, name: 'Head-Up Display', value: nil, msrp: nil},
  {id: 19, name: 'Seat Memory', value: nil, msrp: nil},
  {id: 20, name: 'Power Seat', value: nil, msrp: nil},

  {id: 21, name: 'aux', value: nil, msrp: nil},
  {id: 22, name: 'Cruise Control', value: nil, msrp: nil},
  {id: 23, name: 'Alloy Wheels', value: nil, msrp: nil},
  {id: 24, name: 'Cool Seat', value: nil, msrp: nil},

  {id: 25, name: 'Daytime Running Lights', value: nil, msrp: nil},
  {id: 26, name: 'Keyless Start', value: nil, msrp: nil},
  {id: 27, name: 'Power Windows', value: nil, msrp: nil},
  {id: 28, name: 'XM Satellite', value: nil, msrp: nil},
  {id: 29, name: 'Dual Air Bags', value: nil, msrp: nil},
  {id: 30, name: 'Full Leather', value: nil, msrp: nil},
  {id: 31, name: 'Bi-HID Headlights', value: nil, msrp: nil},
  {id: 32, name: 'Rear Spoiler', value: nil, msrp: nil},
])

Expert.create!([
  {
    name: "Ricky",
    phone: "6264755333",
    email: "ricky@tripalink.com",
    password: "123123123",
    avatar: open("https://s3-us-west-1.amazonaws.com/tplavatar/ricky.jpg"),
    experience: 25,
    address: "11210 Elliott Avenue",
    city: "El Monte",
    state: "CA",
    zipcode: 91733,
    lat: 34.058209,
    lng: -118.031858
  },
  {
    name: "Lucas",
    phone: "8184768601",
    email: "lucastripalink@gmail.com",
    password: "123123123",
    avatar: open("https://s3-us-west-1.amazonaws.com/tplavatar/Lucas.jpg"),
    experience: 25,
    address: "10989 Ventura Boulevard",
    city: "Studio City",
    state: "CA",
    zipcode: 91604,
    lat: 34.1406821,
    lng: -118.3698829
  },
  # {
  #   name: "Expert 3",
  #   phone: "1234567890",
  #   email: "expert_3@tpl.com",
  #   password: "123123123",
  #   avatar: open("https://s3-us-west-1.amazonaws.com/tplavatar/expert.jpg"),
  #   lat: 34.0323859,
  #   lng: -118.291034
  # }
])

# Expert.create!([
#   {
#     name: "", phone: "", experience: 0,
#     address: "", city: "", state: "", zipcode: "",
#     organization: "", biograph: "", rating: 0.0, avatar: "",
#     work_zipcode: "", work_radius: 20,
#     jwt_token: "",
#     email: "yg657@nyu.edu", password: "123123123",
#     reset_password_token: nil, reset_password_sent_at: nil,
#     remember_created_at: nil,
#     sign_in_count: 1,
#     current_sign_in_at: "2015-07-21 22:55:21", last_sign_in_at: "2015-07-21 22:55:21",
#     current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"
#   },
#   {
#     name: "Yu Guo", phone: "123123123", experience: 0,
#     address: "", city: "", state: "", zipcode: "",
#     organization: "", biograph: "", rating: 0.0, avatar: "",
#     work_zipcode: "", work_radius: 20,
#     jwt_token: "",
#     email: "nyuguoyu@gmail.com", password: "123123123",
#     reset_password_token: nil, reset_password_sent_at: nil,
#     remember_created_at: "2015-07-22 16:25:55",
#     sign_in_count: 17,
#     current_sign_in_at: "2015-07-22 16:25:55", last_sign_in_at: "2015-07-22 15:54:00",
#     current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1",
#     lat: 37.4224216,
#     lng: -122.0842603
#   }
# ])

today = Date.today
Expert.all.each do |expert|
  today.upto(today+3.month) do |date|
    (9..19).step(2) do |hour|
      TimeWindow.create!(
        expert_id: expert.id,
        date: date,
        hour: hour,
        status: 1
      )
    end
  end
end

User.create!(name: "Anonymous",
             email: "anon@tripalink.com",
             phone: "8888888888",
             password: "123123123",
             avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user8.jpg"
            )

User.create!(name: "Test",
             email: "test@tpl.com",
             phone: "8888888888",
             password: "123123123",
             avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user8.jpg"
            )

# User.create!(name: "User 2",
#              email: "user_2@tripalink.com",
#              phone: "1234567890",
#              password: "123123123",
#              avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user7.jpg"
#             )

# User.create!(name: "User 3",
#              email: "user_3@tripalink.com",
#              phone: "1234567890",
#              password: "123123123",
#              avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user8.jpg"
#             )

# User.create!([
#   {name: "John Smith", phone: "2138806817", password: "123123123", avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user1.jpg", provider: nil, uid: nil, jwt_token: "", email: "johnsmith@gmail.com"},
#   {name: "Snow White", phone: "2138806817", password: "123123123", avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user2.jpg", provider: nil, uid: nil, jwt_token: "", email: "snowwhite@gmail.com"},
#   {name: "Michael Jackson", phone: "2132132333", password: "123123123", avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user3.jpg", provider: nil, uid: nil, jwt_token: "", email: "michaeljackson@tripalink.com"},
#   {name: "Indiana Jones", phone: "2133332333", password: "123123123", avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user4.jpg", provider: nil, uid: nil, jwt_token: "", email: "indianajones@tripalink.com"},
#   {name: "Donghao Li", phone: "1231234567", password: "123123123", avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user5.jpg", provider: nil, uid: nil, jwt_token: "", email: "donghaoli@tripalink.com"},
#   {name: "", phone: "", admin: false, avatar: "", provider: nil, uid: nil, jwt_token: "", email: "yg657@nyu.edu", password: "123123123", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 1, current_sign_in_at: "2015-07-21 22:53:50", last_sign_in_at: "2015-07-21 22:53:50", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"},
#   {name: "Yu Guo", phone: "1231234567", admin: false, avatar: "https://s3-us-west-1.amazonaws.com/tplavatar/user5.jpg", provider: nil, uid: nil, jwt_token: "", email: "nyuguoyu@gmail.com", password: "123123123", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, sign_in_count: 2, current_sign_in_at: "2015-07-22 01:19:38", last_sign_in_at: "2015-07-21 16:36:24", current_sign_in_ip: "127.0.0.1", last_sign_in_ip: "127.0.0.1"}
# ])

# SellRequest.create!([
#  {id: 7, year: 2013, make: "BMW", model: "3-series", body_style: "Sedan", trim: "Basic", mileage: 999, license_plate: "77777", plate_state: "CA", other_info: "No comment", created_at: "2015-06-16 00:07:51", updated_at: "2015-06-16 00:07:51", user_id: 2}
# ])

# SellRequest.create!([
#   {id: 1, year: 2013, make: "BMW", model: "3-series", body_style: "Sedan", trim: "Basic", mileage: 999, license_plate: "77777", plate_state: "CA", other_info: "No comment", user_id: 3},
#   {id: 2, year: 2014, make: "BMW", model: "911", body_style: "Sedan", trim: "Sports", mileage: 12345, license_plate: "11111", plate_state: "CA", other_info: "", user_id: 4},
#   {id: 3, year: 2013, make: "Porsche", model: "911", body_style: "Sedan", trim: "Basic", mileage: 5432, license_plate: "2222", plate_state: "CA", other_info: "", user_id: 5},
#   {id: 4, year: 2012, make: "BMW", model: "3-series", body_style: "Coupe", trim: "Sports", mileage: 3333, license_plate: "23333", plate_state: "CA", other_info: "", user_id: 6},
#   {id: 5, year: 2011, make: "BMW", model: "911", body_style: "Sedan", trim: "Sports", mileage: 70, license_plate: "34567", plate_state: "CA", other_info: "", user_id: 7},
#   {id: 6, year: 2009, make: "Porsche", model: "911", body_style: "Sedan", trim: "Basic", mileage: 999, license_plate: "6666", plate_state: "CA", other_info: "", user_id: 8}
# ])

# cayenne = CarModel.find_by(make: "Porsche", model: "Cayenne", year: 2013, trim: "Base")
# if cayenne == nil
#   cayenne = CarModel.create!({id: 37037, make: "Porsche", make_niceName: "porsche", model: "Cayenne", modelId: "Porsche_Cayenne", model_niceName: "cayenne", year: 2013, body: "SUV", fuel: nil, trim: "Base", modelName: "Cayenne SUV", styleName: "4dr SUV AWD (3.6L 6cyl 6M)", styleId: 200427034, styleURL: "http://www.edmunds.com/porsche/cayenne/2013/st-200427034/features-specs/", fuelEconomy: "15/22 mpg", transmission: "6-speed Manual", basicWarranty: "4 Yr./ 50000 Mi.", bluetooth: "Yes", heatedSeats: "Yes", engineType: "Gas", totalSeating: "5", cylinders: "V6", driveTrain: "All Wheel Drive", navigation: "Not Available", created_at: "2015-06-02 01:00:36", updated_at: "2015-06-02 01:53:18"})
# end
# v = cayenne.cars.create!(mileage: 19002, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray", price: 53000, price_edmunds_retail: 18859, price_dealer_retail: 26000,vin: "WP1AA2A27DLA06474", address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: 90089, sold: false, interior_color_id: 1, exterior_color_id: 15, status: 2)
# v.car_pictures.create!([
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/1.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/2.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/3.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/4.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/5.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/6.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/7.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/8.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/9.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/10.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/11.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/12.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/13.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/14.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/15.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/16.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/17.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/18.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/19.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/20.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/21.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/22.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/23.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/24.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/25.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/26.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/27.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/28.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/29.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/30.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/31.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/chelyer/32.jpg")}])

# ml350 = CarModel.find_by(make: "Mercedes-Benz", model: "M-Class", year: 2011, trim: "ML350")
# if ml350 == nil
#   ml350 = CarModel.create!({id: 31530, make: "Mercedes-Benz", make_niceName: "mercedes-benz", model: "M-Class", modelId: "Mercedes_Benz_M_Class", model_niceName: "m-class", year: 2011, body: "SUV", fuel: nil, trim: "ML350", modelName: "M-Class SUV", styleName: "ML350 4dr SUV (3.5L 6cyl 7A)", styleId: 101344621, styleURL: "http://www.edmunds.com/mercedes-benz/m-class/2011/st-101344621/features-specs/", fuelEconomy: "16/20 mpg", transmission: "7-speed Shiftable Automatic", basicWarranty: "4 Yr./ 50000 Mi.", bluetooth: "Yes", heatedSeats: "Yes", engineType: "Gas", totalSeating: "5", cylinders: "V6", driveTrain: "Rear Wheel Drive", navigation: "", created_at: "2015-06-02 01:00:19", updated_at: "2015-06-12 01:10:23"})
# end
# v = ml350.cars.create!(mileage: 50000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Light blue", price: 27800, price_edmunds_retail: 18859, price_dealer_retail: 26000,vin: "4JGBB5GB5BA662205", address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: 90089, sold: false, interior_color_id: 17, exterior_color_id: 11, status: 4)
# v.car_pictures.create!([
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/01.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/02.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/03.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/04.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/05.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/06.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/07.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/08.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/09.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/10.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/11.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/12.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/13.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/14.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/15.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/16.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/17.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/18.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/19.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/20.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/21.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/22.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/23.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/24.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/25.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/26.jpg")},
#   {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/infinity/27.jpg")}])

# titanium = CarModel.find_by(make: "Ford", model: "Focus", year: 2014, body: "Sedan", trim: "Titanium")
# if titanium == nil
#   titanium = CarModel.create!({id: 17371, make: "Ford", make_niceName: "ford", model: "Focus", modelId: "Ford_Focus", model_niceName: "focus", year: 2014, body: "Sedan", fuel: nil, trim: "Titanium", modelName: "Focus Sedan", styleName: "Titanium 4dr Sedan (2.0L 4cyl 6AM)", styleId: 200473301, styleURL: "http://www.edmunds.com/ford/focus/2014/st-200473301/features-specs/", fuelEconomy: "26/37 mpg", transmission: "6-speed Automated Manual", basicWarranty: "3 Yr./ 36000 Mi.", bluetooth: "Yes", heatedSeats: "Yes", engineType: "Flex-fuel (ffv)", totalSeating: "5", cylinders: "Inline 4", driveTrain: "Front Wheel Drive", navigation: "", created_at: "2015-06-02 00:59:37", updated_at: "2015-06-02 01:42:51"})
# end
# v = titanium.cars.create!(mileage: 7826, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White", price: 18500, price_edmunds_retail: 18859, price_dealer_retail: 26000,vin: "1FADP3N26EL110815", address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: 90089, sold: false, interior_color_id: 1, exterior_color_id: 3, status: 1,
#                           checklist: ["v1.0", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "20", "20", "20", "20", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "13", "13", "13", "13", "37", "37", "37", "37", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true"])
# v.car_pictures.create!([
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350285-spaq91e6u5wmi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5353.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350291-3r4iset5kizilik9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5354.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350293-34fimjvv5lmobt9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5355.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350296-29lo1hso1a5lv7vi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5356.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350299-5hgdltu66jpaxlxr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5357.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350302-rwkqi4o04die8kt9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5358.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350305-non4j7z4vppn9udi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5359.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350307-42t4krui5dzvkj4i-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5360.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350309-d7ejvl7sp9icnmi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5361.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350311-thfigo93i8bprpb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5362.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350313-ob7urwfuma38fr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5363.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350315-16bxvyujbsinqaor-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5364.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350317-xmf436c98g5y7gb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5365.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350319-b1ymzau1exj8xgvi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5367.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350321-ov3z6d2ur270t3xr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5368.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350323-npedbzq49k0ggb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5369.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350325-jllas788semi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5370.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350327-bh8ndwbg88yta9k9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5371.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350329-z121i48hahgaxlxr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5374.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350331-okrp38u5dujcq5mi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5375.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350341-yf1jexxtbk6xyldi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5376.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350344-ku02zle68zg2e29-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5377.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350347-b7a6h5od2q1o47vi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5378.JPG")}])
# v.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/AutoCheck+Vehicle+History+Reports_+VIN+Check%2C+Your+Report.pdf"
# v.carfax = "https://s3-us-west-1.amazonaws.com/tplreports/CARFAX+Vehicle+History+Report+for+this+2014+FORD+FOCUS+TITANIUM_+1FADP3N26EL110815.pdf"
# v.save


# lexus = CarModel.find_by(make: "Lexus", model: "IS 250", year: 2013, body: "Sedan", trim: "Base", styleName: "4dr Sedan AWD (2.5L 6cyl 6A)")
# if lexus == nil
#   lexus = CarModel.create!({id: 28743, make: "Lexus", make_niceName: "lexus", model: "IS 250", modelId: "Lexus_IS_250", model_niceName: "is-250", year: 2013, body: "Sedan", fuel: nil, trim: "Base", modelName: "IS 250 Sedan", styleName: "4dr Sedan AWD (2.5L 6cyl 6A)", styleId: 200432927, styleURL: "http://www.edmunds.com/lexus/is-250/2013/st-200432927/features-specs/", fuelEconomy: "21/27 mpg", transmission: "6-speed Shiftable Automatic", basicWarranty: "4 Yr./ 50000 Mi.", bluetooth: "Yes", heatedSeats: "Yes", engineType: "Gas", totalSeating: "5", cylinders: "V6", driveTrain: "All Wheel Drive", navigation: "Not Available", created_at: "2015-06-02 01:00:11", updated_at: "2015-06-02 01:54:46"})
# end
# v = lexus.cars.create!(mileage: 33000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray", price: 26500,price_edmunds_retail: 18859, price_dealer_retail: 26000, vin: "JTHBF5C22D5188464", address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: 90089, sold: false, interior_color_id: 1, exterior_color_id: 3, status: 1,
#                           checklist: ["v1.0", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "20", "20", "20", "20", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "13", "13", "13", "13", "37", "37", "37", "37", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true"])
# v.car_pictures.create!([
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350285-spaq91e6u5wmi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5353.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350291-3r4iset5kizilik9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5354.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350293-34fimjvv5lmobt9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5355.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350296-29lo1hso1a5lv7vi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5356.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350299-5hgdltu66jpaxlxr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5357.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350302-rwkqi4o04die8kt9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5358.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350305-non4j7z4vppn9udi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5359.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350307-42t4krui5dzvkj4i-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5360.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350309-d7ejvl7sp9icnmi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5361.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350311-thfigo93i8bprpb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5362.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350313-ob7urwfuma38fr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5363.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350315-16bxvyujbsinqaor-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5364.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350317-xmf436c98g5y7gb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5365.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350319-b1ymzau1exj8xgvi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5367.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350321-ov3z6d2ur270t3xr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5368.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350323-npedbzq49k0ggb9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5369.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350325-jllas788semi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5370.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350327-bh8ndwbg88yta9k9-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5371.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350329-z121i48hahgaxlxr-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5374.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350331-okrp38u5dujcq5mi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5375.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350341-yf1jexxtbk6xyldi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5376.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350344-ku02zle68zg2e29-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5377.JPG")},
#       {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/uploads-1430293350347-b7a6h5od2q1o47vi-e80d6d19dde6e87058aedb9a637a1574-resized_IMG_5378.JPG")}])
# v.carfax = "https://s3.amazonaws.com/lxnj/10/CARFAX+Vehicle+History+Report+for+this+2013+LEXUS+IS+250_+JTHBF5C22D5188464.pdf"
# v.save



# Appointment.create!([
#   {appointment_type: 0, address: "1183 W. 36th Pl.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 3, expert_id: 1, time_window_id: 27, car_id: nil},
#   {appointment_type: 0, address: "2318 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90089", user_id: 4, expert_id: 1, time_window_id: 1, car_id: nil},
#   {appointment_type: 0, address: "325 North Chapel Ave.", city: "Alhambra", state: "CA", zipcode: "91801", user_id: 5, expert_id: 1, time_window_id: 45, car_id: nil},
#   {appointment_type: 0, address: "2319 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 6, expert_id: 1, time_window_id: 54, car_id: nil},
#   {appointment_type: 0, address: "3607 Trousdale Parkway", city: "Los Angeles", state: "CA", zipcode: "90089", user_id: 7, expert_id: 1, time_window_id: 65, car_id: nil},
#   {appointment_type: 0, address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: "90089", user_id: 8, expert_id: 1, time_window_id: 49, car_id: nil},
#   {appointment_type: 0, address: "3607 Trousdale Parkway", city: "Los Angeles", state: "CA", zipcode: "90089", user_id: 7, expert_id: 1, time_window_id: 65, car_id: nil},
#   {appointment_type: 0, address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: "90089", user_id: 8, expert_id: 1, time_window_id: 49, car_id: nil},
#   {appointment_type: 0, address: "325 North Chapel Ave.", city: "Alhambra", state: "CA", zipcode: "91801", user_id: 1, expert_id: 1, time_window_id: 45, car_id: 1},
#   {appointment_type: 0, address: "2319 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 1, expert_id: 1, time_window_id: 54, car_id: 2},
#   {appointment_type: 1, address: "1183 W. 36th Pl.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 1, expert_id: 1, time_window_id: 27, car_id: 3},
#   {appointment_type: 0, address: "2319 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 9, expert_id: 2, time_window_id: 54, car_id: 3},
#   {appointment_type: 0, address: "2319 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 9, expert_id: 5, time_window_id: 54, car_id: 3},
#   {appointment_type: 0, address: "2319 Portland St.", city: "Los Angeles", state: "CA", zipcode: "90007", user_id: 9, expert_id: 5, time_window_id: 54, car_id: 1}
# ])


# mini = CarModel.find_by(make: "MINI", model: "Cooper", year: 2012, body: "Convertible", trim: "S", styleName: "S 2dr Convertible (1.6L 4cyl Turbo 6M)")
# if mini == nil
#   mini = CarModel.create!({id: 29402, make: "MINI", make_niceName: "mini", model: "Cooper", modelId: "MINI_Cooper", model_niceName: "cooper", year: 2013, body: "Convertible", fuel: nil, trim: "S", modelName: "Cooper Convertible", styleName: "S 2dr Convertible (1.6L 4cyl Turbo 6M)", styleId: 200434270, styleURL: "http://www.edmunds.com/mini/cooper/2013/st-200434270/features-specs/", fuelEconomy: "26/35 mpg", transmission: "6-speed Manual", basicWarranty: "4 Yr./ 50000 Mi.", bluetooth: "Yes", heatedSeats: "Yes", engineType: "Gas", totalSeating: "4", cylinders: "Inline 4", driveTrain: "Front Wheel Drive", navigation: "Not Available", created_at: "2015-06-02 01:00:13", updated_at: "2015-06-02 01:55:11"})
# end
# m = mini.cars.create!(mileage: 33000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray", price: 26500,price_edmunds_retail: 18859, price_dealer_retail: 26000, vin: "WMWSV3C56CTY19470", address: "University of Southern California", city: "Los Angeles", state: "CA", zipcode: 90089, sold: false, interior_color_id: 1, exterior_color_id: 3, status: 1,
#                        checklist: ["v1.0", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "20", "20", "20", "20", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "13", "13", "13", "13", "37", "37", "37", "37", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true", "true"])

# m.car_pictures.create!([
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/01.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/02.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/03.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/04.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/05.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/06.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/07.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/08.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/09.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/10.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/11.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/12.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/13.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/14.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/15.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/16.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/17.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/18.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/19.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/20.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/21.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/22.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/23.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/24.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/25.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/26.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/27.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/28.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/29.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/30.jpg")},
#                            {picture: open("https://s3-us-west-1.amazonaws.com/tplcarpics/acura/31.jpg")}
#                        ])
# m.carfax = "https://s3.amazonaws.com/lxnj/10/CARFAX+Vehicle+History+Report+for+this+2013+LEXUS+IS+250_+JTHBF5C22D5188464.pdf"
# m.save


# 2008 Acura RDX
model = CarModel.find_by(styleId: 100905540)
car = model.cars.create!(mileage: 62000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Black",
    price: 15000, price_kbb_suggest_retail: 16222, price_kbb_max_retail: 17293,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 750,
    vin: "5J8TB18238A010645",
    address: "", city: "Azusa", state: "CA", zipcode: 91702,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/27.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/28.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/29.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/30.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/31.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2008+Acura+RDX.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2008+Acura+RDX.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 10},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2010 Benz GLK350
model = CarModel.find_by(styleId: 101132522)
car = model.cars.create!(mileage: 41000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "White",
    price: 22000, price_kbb_suggest_retail: 22484, price_kbb_max_retail: 23743,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1100,
    vin: "WDCGG5GB0AF491306",
    address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/27.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Mercedes+Benz+GLK.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Benz+GLK.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 9},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2010 BMW 328i
model = CarModel.find_by(styleId: 101200939)
car = model.cars.create!(mileage: 47000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Gray",
    price: 14000, price_kbb_suggest_retail: 17009, price_kbb_max_retail: 18451,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 700,
    vin: "WBAPH5G51ANM35763",
    address: "", city: "Woodland Hills", state: "CA", zipcode: 91367,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 4)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/01.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/02.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/03.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/04.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/05.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/06.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/07.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/08.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/09.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/10.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/11.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/12.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/13.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/14.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/15.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/16.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/17.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/18.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/19.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/20.JPG"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/21.JPG"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+BMW+328i.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+BMW+328i.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2010 Chevelot Malibu LT
model = CarModel.find_by(styleId: 101184444)
car = model.cars.create!(mileage: 52000, owner_id: 1, inspector_id: 1, int_color: "White", ext_color: "White",
    price: 10000, price_kbb_suggest_retail: 12732, price_kbb_max_retail: 13509,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
    vin: "1G1ZC5EB8A4101120",
    address: "", city: "Alhambra", state: "CA", zipcode: 91801,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/25.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Chevrolet+Malibu+LT.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Chverolet+Malibu+LT.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2011 Honda CRV
model = CarModel.find_by(styleId: 101358302)
car = model.cars.create!(mileage: 29000, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "White",
    price: 16000, price_kbb_suggest_retail: 18809, price_kbb_max_retail: 19914,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 800,
    vin: "5J6RE3H45BL047070",
    address: "", city: "Rowland Heights", state: "CA", zipcode: 91748,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/18.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Honda+CRV.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Honda+Crv+SE.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2012 Honda Accord EX-L
model = CarModel.find_by(styleId: 101403697)
car = model.cars.create!(mileage: 26000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "White",
    price: 17000, price_kbb_suggest_retail: 18138, price_kbb_max_retail: 19425,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 850,
    vin: "1HGCP3F85CA035575",
    address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/25.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Honda+Accord+EX-L.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Honda+Accord+EX-L.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 10},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2012 Honda Civic
model = CarModel.find_by(styleId: 101384824)
car = model.cars.create!(mileage: 21000, owner_id: 1, inspector_id: 1, int_color: "Silver", ext_color: "White",
    price: 13000, price_kbb_suggest_retail: 15428, price_kbb_max_retail: 16860,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
    vin: "2HGFG3B84CH550040",
    address: "", city: "Azusa", state: "CA", zipcode: 91702,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/25.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Honda+Civic+Coupe+EX-L.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Honda+Civic+Coupe+EX-L.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 10},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2012 MINI Cooper S
model = CarModel.find_by(styleId: 101395835)
car = model.cars.create!(mileage: 48000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
    price: 15000, price_kbb_suggest_retail: 15383, price_kbb_max_retail: 16499,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 750,
    vin: "WMWSV3C56CTY19470",
    address: "", city: "Azusa", state: "CA", zipcode: 91702,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/27.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Mini+coopers.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Mini+Cooper+S.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2012 VW Passat
model = CarModel.find_by(styleId: 101392526)
car = model.cars.create!(mileage: 47000, owner_id: 1, inspector_id: 1, int_color: "White", ext_color: "Beige",
    price: 13000, price_kbb_suggest_retail: 13593, price_kbb_max_retail: 14392,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
    vin: "1VWBP7A3XCC010833",
    address: "", city: "Calabasas", state: "CA", zipcode: 91302,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/27.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/28.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/29.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+VW+Passat+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+VW+Passt+SE.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2013 Chrysler
model = CarModel.find_by(styleId: 200437794)
car = model.cars.create!(mileage: 21000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
    price: 12800, price_kbb_suggest_retail: 15200, price_kbb_max_retail: 16105,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 640,
    vin: "1C3CCBBBXDN542556",
    address: "", city: "Pasadena", state: "CA", zipcode: 91101,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 5)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/27.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/28.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/29.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/30.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/31.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/32.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Chrysler+200+Touring.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Chysler+200+Touring.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2013 Focus
model = CarModel.find_by(styleId: 200421422)
car = model.cars.create!(mileage: 38000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Silver",
    price: 13600, price_kbb_suggest_retail: 14806, price_kbb_max_retail: 15978,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 680,
    vin: "1FADP3K29DL337189",
    address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 2)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/27.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/28.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/29.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Ford+Focus.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Ford+Focus+SE.pdf"
car.save


# 2013 Infinity G37 Final
model = CarModel.find_by(styleId: 200437359)
car = model.cars.create!(mileage: 31000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
    price: 22500, price_kbb_suggest_retail: 23774, price_kbb_max_retail: 25608,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1125,
    vin: "JN1CV6AP4DM713188",
    address: "", city: "Pasadena", state: "CA", zipcode: 91101,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/27.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Infiniti+G37+Journey.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Inifiti+G37+Journey.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 10},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 19},
    {car_id: car.id, feature_id: 20},
])
car.save


# 2013 VW Jetta S
model = CarModel.find_by(styleId: 200471614)
car = model.cars.create!(mileage: 45000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
    price: 10500, price_kbb_suggest_retail: 12533, price_kbb_max_retail: 13694,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 525,
    vin: "3VW2K7AJ5DM358066",
    address: "", city: "Torrance", state: "CA", zipcode: 90502,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/25.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+VW+Jetta+S.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+VW+Jetta+S.pdf"
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])
car.save


# 2011 Honda Civic LX
model = CarModel.find_by(styleId: 101357837)
car = model.cars.create!(mileage: 60000, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "White",
    price: 12000, price_kbb_suggest_retail: 12800, price_kbb_max_retail: 13500,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 600,
    vin: "2HGFA1F5XBH527589",
    address: "", city: "El monte", state: "CA", zipcode: 91733,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/19.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Honda+Civc+LX.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Honda+Civic+LX+6Wmiles.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
])


# 2013 Audi A5 Premium Plus
model = CarModel.find_by(styleId: 200722296)
car = model.cars.create!(mileage: 14000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
    price: 34000, price_kbb_suggest_retail: 35500, price_kbb_max_retail: 36942,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1775,
    vin: "WAULFAFR1DA039572",
    address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/16.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Audi+A5+Premium+Plus.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Audi+A5+Premium+Plus.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])


# 2013 Chysler 200 Limited
model = CarModel.find_by(styleId: 200437796)
car = model.cars.create!(mileage: 15000, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "White",
    price: 14500, price_kbb_suggest_retail: 15800, price_kbb_max_retail: 17780,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 725,
    vin: "1C3CCBCG3DN751955",
    address: "", city: "Monterey Park", state: "CA", zipcode: 91754,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/21.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/22.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/23.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/24.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/25.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/26.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/27.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Chrysler+200+limited.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Chrysler+200+Limit+Sport.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 14},
])


# 2013 Mini Roadster Cooper S
model = CarModel.find_by(styleId: 200438517)
car = model.cars.create!(mileage: 21000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Blue",
    price: 20000, price_kbb_suggest_retail: 22891, price_kbb_max_retail: 24342,
    price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1000,
    vin: "WMWSY3C50DT594102",
    address: "", city: "Azusa", state: "CA", zipcode: 91702,
    sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/01.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/02.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/03.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/04.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/05.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/06.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/07.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/08.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/09.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/10.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/11.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/12.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/13.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/14.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/15.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/16.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/17.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/18.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/19.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/20.jpg"},
    {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/21.jpg"},
])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013Mini.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Mini+Roadster+Cooper+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 14},
])



# 2011 Toyota Camry
model = CarModel.find_by(styleId: 101275055)
car = model.cars.create!(mileage: 33000, owner_id: 1, inspector_id: 1, int_color: "Silver", ext_color: "Black",
                         price: 11600, price_kbb_suggest_retail: 14265, price_kbb_max_retail: 15022,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 580,
                         vin: "4T4BF3EK3BR154735",
                         address: "", city: "Hollywood", state: "CA", zipcode: 90028,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 4)
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/22.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Toyota+Camry+LE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Toyota+Camry+LE.pdf"
car.save
CarFeatureRelation.create!([
                               {car_id: car.id, feature_id: 1},
                               {car_id: car.id, feature_id: 12},
                           ])



#2012 Honda Accord LX
model = CarModel.find_by(styleId: 101403690)
car = model.cars.create!(mileage: 31000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Gray",
                         price: 12800, price_kbb_suggest_retail: 14300, price_kbb_max_retail: 15360,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 640,
                         vin: "1HGCP2F38CA208987",
                         address: "", city: "Azusa", state: "CA", zipcode: 91702,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/24.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Honda+Accord+LX.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Honda+Accord+LX.pdf"
car.save
CarFeatureRelation.create!([
                               {car_id: car.id, feature_id: 12},
                           ])


#2014 Toyota Corolla L
model = CarModel.find_by(styleId: 200487333)
car = model.cars.create!(mileage: 10600, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 13000, price_kbb_suggest_retail: 15300, price_kbb_max_retail: 16790,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
                         vin: "5J6RM3H50CL044235",
                         address: "", city: "Azusa", state: "CA", zipcode: 91702,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/24.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Toyota+Corolla+L.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Toyota+Corolla+L.pdf"
car.save
CarFeatureRelation.create!([
                               {car_id: car.id, feature_id: 12},
                           ])


# 2012 Honda CRV EX
model = CarModel.find_by(styleId: 101418795)
car = model.cars.create!(mileage: 29000, owner_id: 1, inspector_id: 2, int_color: "Beige", ext_color: "Silver",
                         price: 18500, price_kbb_suggest_retail: 18980, price_kbb_max_retail: 21137,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 925,
                         vin: "5YFBURHE9EP031701",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/21.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Honda+CRV+EX-2.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Honda+CRV+EX.pdf"
car.save
CarFeatureRelation.create!([
                               {car_id: car.id, feature_id: 6},
                               {car_id: car.id, feature_id: 11},
                               {car_id: car.id, feature_id: 12},
                           ])



# 2010 GLK 350
model = CarModel.find_by(styleId: 101132522)
car = model.cars.create!(mileage: 39000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 21000, price_kbb_suggest_retail: 23700, price_kbb_max_retail: 25000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1050,
                         vin: "WDCGG5GB3AF402845",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 5)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/23.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Benz+GLK350.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Benz+GLK+Black.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 9},
])


# 2011 BMW M3 Convertible
model = CarModel.find_by(styleId: 101287990)
car = model.cars.create!(mileage: 27000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 42500, price_kbb_suggest_retail: 45000, price_kbb_max_retail: 46784,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 2125,
                         vin: "WBSDX9C55BE784120",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/29.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+BMW+M3.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+BMW+M3.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 19},
])


# 2012 Benz C63
model = CarModel.find_by(styleId: 101377609)
car = model.cars.create!(mileage: 41000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 42000, price_kbb_suggest_retail: 45000, price_kbb_max_retail: 47500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 2100,
                         vin: "WDDGF7HBXCA662149",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/29.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Benz+C63+AMG.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Benz+C63+AMG.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 19},
])


# 2012 Benz C250
model = CarModel.find_by(styleId: 101336914)
car = model.cars.create!(mileage: 34500, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Blue",
                         price: 22000, price_kbb_suggest_retail: 23000, price_kbb_max_retail: 25500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1100,
                         vin: "WDDGF4HB2CA622565",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/28.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Benz+C250+Luxury.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Benz+C250.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 19},
])


# 2013 BMW 328i
model = CarModel.find_by(styleId: 200423469)
car = model.cars.create!(mileage: 30000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 24000, price_kbb_suggest_retail: 26490, price_kbb_max_retail: 28331,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1200,
                         vin: "WBA3A5C59DF602426",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+BMW+328i.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+BMW+328i.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])



# 2007 Mini Cooper S
model = CarModel.find_by(styleId: 100837391)
car = model.cars.create!(mileage: 49500, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 8500, price_kbb_suggest_retail: 9678, price_kbb_max_retail: 10565,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
                         vin: "WMWMF73567TL91029",
                         address: "", city: "Temple City", state: "CA", zipcode: 91780,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2007+Mini+Cooper+S+Inspection.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2007+Mini+Cooper+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])


# 2010 VW New Beetle
model = CarModel.find_by(styleId: 101286160)
car = model.cars.create!(mileage: 72000, owner_id: 1, inspector_id: 2, int_color: "White", ext_color: "White",
                         price: 10000, price_kbb_suggest_retail: 14055, price_kbb_max_retail: 15465,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
                         vin: "3VWRG3AL0AM006390",
                         address: "", city: "Calabasas", state: "CA", zipcode: 91302,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/23.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+VW+New+Beetle.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+VW+New+Beetle.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])


# 2013 Audi A3
model = CarModel.find_by(styleId: 200722279)
car = model.cars.create!(mileage: 34000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Gray",
                         price: 24000, price_kbb_suggest_retail: 26882, price_kbb_max_retail: 27418,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1200,
                         vin: "WAUKEAFM8DA025297",
                         address: "", city: "Santa Monica", state: "CA", zipcode: 90025,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Audi+A3+Inspection.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Audi+A3.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 9},
    {car_id: car.id, feature_id: 20},
])



# 2012 BMW 328i
model = CarModel.find_by(styleId: 101411139)
car = model.cars.create!(mileage: 43000, owner_id: 1, inspector_id: 1, int_color: "Red", ext_color: "Black",
                         price: 22500, price_kbb_suggest_retail: 24600, price_kbb_max_retail: 26799,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1125,
                         vin: "WBAKE5C5XCJ106501",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/20.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+BMW+328i.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+BMW+328i+Redinter.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 19},
])



# 2013 Audi A4
model = CarModel.find_by(styleId: 200722272)
car = model.cars.create!(mileage: 30000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Gray",
                         price: 24500, price_kbb_suggest_retail: 28433, price_kbb_max_retail: 28833,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1225,
                         vin: "WAUEFAFL2DA080488",
                         address: "", city: "Azusa", state: "CA", zipcode: 91702,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 5)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/27.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Audi+A4.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Audi+A4.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
])


# 2013 Mini Cooper
model = CarModel.find_by(styleId: 200434267)
car = model.cars.create!(mileage: 13000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
                         price: 16000, price_kbb_suggest_retail: 16500, price_kbb_max_retail: 17500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 800,
                         vin: "WMWSU3C57DT678073",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Mini+Cooper.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Mini+Cooper+.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
])



# 2011 VW CC Sport
model = CarModel.find_by(styleId: 101336021)
car = model.cars.create!(mileage: 42000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Gray",
                         price: 14000, price_kbb_suggest_retail: 13990, price_kbb_max_retail: 14240,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 700,
                         vin: "WVWMP7AN9BE733346",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+VW+CC+Sport+Inspection.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+VW+CC+Sport.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 8},
])


# 2012 VW Jetta Base-s
model = CarModel.find_by(styleId: 101393732)
car = model.cars.create!(mileage: 66600, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
                         price: 9300, price_kbb_suggest_retail: 11269, price_kbb_max_retail: 12278,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
                         vin: "3VW2K7AJ0CM423923",
                         address: "", city: "Torrance", state: "CA", zipcode: 90502,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+VW+Jetta+Base-s.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+VW+Jetta+Base.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])




# 2010 Honda Civic EX
model = CarModel.find_by(styleId: 101209927)
car = model.cars.create!(mileage: 31400, owner_id: 1, inspector_id: 2, int_color: "Gray", ext_color: "Blue",
                         price: 11800, price_kbb_suggest_retail: 13983, price_kbb_max_retail: 15456,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 590,
                         vin: "2HGFG1B85AH520417",
                         address: "", city: "Rosemead", state: "CA", zipcode: 91770,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Honda+Civic+EX+Inspection.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Honda+Civic+EX.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])





# 2012 Benz C250 Sports
model = CarModel.find_by(styleId: 101336914)
car = model.cars.create!(mileage: 33000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 21700, price_kbb_suggest_retail: 22500, price_kbb_max_retail: 23690,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1085,
                         vin: "WDDGF4HB0CA729372",
                         address: "", city: "Rowland Heights", state: "CA", zipcode: 91748,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/31.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/32.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/33.jpg"},

                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Benz+C250+Sports.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Benz+C250+Sports.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 20},
])


# 2012 Chevelot Camero LT RS
model = CarModel.find_by(styleId: 101395586)
car = model.cars.create!(mileage: 28000, owner_id: 1, inspector_id: 1, int_color: "Red", ext_color: "Black",
                         price: 21000, price_kbb_suggest_retail: 25061, price_kbb_max_retail: 26629,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1050,
                         vin: "2G1FC1E33C9142579",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/21.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Chevrolet+Camaro+LT+RS.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Chverolet+Camero+LT+RS.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 13},
    {car_id: car.id, feature_id: 20},
])






# 2012 VW Beetle
model = CarModel.find_by(styleId: 101396953)
car = model.cars.create!(mileage: 14000, owner_id: 1, inspector_id: 1, int_color: "Bronze", ext_color: "Black",
                         price: 14000, price_kbb_suggest_retail: 14500, price_kbb_max_retail: 15600,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 700,
                         vin: "3VWJP7AT7CM664800",
                         address: "", city: "Rowland Heights", state: "CA", zipcode: 91748,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/22.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+VW+Beetle+2.5L.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+VW+Bettles.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 20},
])



# 2014 Toyota Camry SE
model = CarModel.find_by(styleId: 200485962)
car = model.cars.create!(mileage: 11760, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
                         price: 16900, price_kbb_suggest_retail: 20100, price_kbb_max_retail: 21500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 845,
                         vin: "4T1BF1FK7EU770648",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/27.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Toyota+Camry+SE+checklist.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Toyota+Camry+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 20},
])




# 2012 VW Jetta SE
model = CarModel.find_by(styleId: 101393776)
car = model.cars.create!(mileage: 52000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Blue",
                         price: 11500, price_kbb_suggest_retail: 12548, price_kbb_max_retail: 13819,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 575,
                         vin: "3VWDP7AJ7CM315115",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+VW+Jetta+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+VW+Jetta+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 12},
])


# 2014 Ford Fusion, should be 2013 Ford Fusion
model = CarModel.find_by(styleId: 101419249)
car = model.cars.create!(mileage: 25600, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 13200, price_kbb_suggest_retail: 15200, price_kbb_max_retail: 16200,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 660,
                         vin: "3FA6P0G79DR367063",
                         address: "", city: "Baldwin Park", state: "CA", zipcode: 91706,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/22.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Ford+Fusion+S.pdf"
car.autocheck = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Ford+Fusion+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 20},
])




# 2013 VW PASSAT
model = CarModel.find_by(styleId: 200431136)
car = model.cars.create!(mileage: 35600, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "Black",
                         price: 11100, price_kbb_suggest_retail: 12838, price_kbb_max_retail: 14289,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 555,
                         vin: "1VWAP7A34DC028922",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/19.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+VW+Passat+S+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+VW+Passat+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])



# 2009 Mercedes-Benz C350
model = CarModel.find_by(styleId: 101039306)
car = model.cars.create!(mileage: 41554, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Black",
                         price: 19500, price_kbb_suggest_retail: 20714, price_kbb_max_retail: 22605,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 975,
                         vin: "WDDGF56X69R050849",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/23.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2009+Mercedes-+Benz+C350+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2009+Mercedes-+Benz+C350.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 19},
    {car_id: car.id, feature_id: 20},
])


# 2011 VW Jetta S
model = CarModel.find_by(styleId: 101362267)
car = model.cars.create!(mileage: 22187, owner_id: 1, inspector_id: 2, int_color: "Gray", ext_color: "White",
                         price: 11000, price_kbb_suggest_retail: 11429, price_kbb_max_retail: 12686,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 550,
                         vin: "3VW2K7AJ1BM364802",
                         address: "", city: "San Gabrial", state: "CA", zipcode: 91776,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/23.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+VW+Jetta+S+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+VW+Jetta+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])




# 2014 Toyota Corolla LE
model = CarModel.find_by(styleId: 200487343)
car = model.cars.create!(mileage: 43343, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 13000, price_kbb_suggest_retail: 14263, price_kbb_max_retail: 15968,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
                         vin: "5YFBURHE6EP051355",
                         address: "", city: "San Gabrial", state: "CA", zipcode: 91776,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Toyota+Corolla+LE+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Toyota+Corolla+LE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
])


# 2008 Mini Cooper
model = CarModel.find_by(styleId: 100955393)
car = model.cars.create!(mileage: 35422, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 9500, price_kbb_suggest_retail: 9408, price_kbb_max_retail: 10728,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
                         vin: "WMWMF33578TT65842",
                         address: "", city: "Rosemead", state: "CA", zipcode: 91770,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/21.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2008+Mini+Cooper+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2008+Mini+Cooper.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
])



# 2011 VW Tiguan SE
model = CarModel.find_by(styleId: 101338828)
car = model.cars.create!(mileage: 54500, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 13000, price_kbb_suggest_retail: 15500, price_kbb_max_retail: 16300,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
                         vin: "WVGAV7AX6BW560286",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/29.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+VW+Tiguan.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+VW+Tiguan.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 15},
])


# 2012 VW Passat SE
model = CarModel.find_by(styleId: 101392528)
car = model.cars.create!(mileage: 29700, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 13000, price_kbb_suggest_retail: 15600, price_kbb_max_retail: 16600,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 650,
                         vin: "1VWBP7A37CC004083",
                         address: "", city: "Ontario", state: "CA", zipcode: 91762,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 4)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+VW+Passat+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+VW+Passat+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
])



# 2013 Benz GLK350
model = CarModel.find_by(styleId: 200436081)
car = model.cars.create!(mileage: 17000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Silver",
                         price: 30000, price_kbb_suggest_retail: 33480, price_kbb_max_retail: 34960,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1500,
                         vin: "WDCGG8JB1DF941169",
                         address: "", city: "Walnut", state: "CA", zipcode: 91789,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Benz+GLK350.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Benz+GLK350.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
])


# 2013 Honda CRV
model = CarModel.find_by(styleId: 200436017)
car = model.cars.create!(mileage: 23200, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "Blue",
                         price: 18000, price_kbb_suggest_retail: 22800, price_kbb_max_retail: 23700,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 900,
                         vin: "3CZRM3H36DG711693",
                         address: "", city: "Walnut", state: "CA", zipcode: 91789,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/31.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Honda+CRv.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Honda+CRV.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])



# 2010 Infiniti G37
model = CarModel.find_by(styleId: 101286690)
car = model.cars.create!(mileage: 59000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
                         price: 15000, price_kbb_suggest_retail: 21600, price_kbb_max_retail: 23246,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 750,
                         vin: "JN1CV6AR5AM450674",
                         address: "", city: "Eastvale", state: "CA", zipcode: 92880,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/29.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Infiniti+Anniversary+Edition+.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Infiniti+G37.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 15},
    {car_id: car.id, feature_id: 20},
])


# 2010 VW CC Sport
model = CarModel.find_by(styleId: 101215300)
car = model.cars.create!(mileage: 52000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "White",
                         price: 12500, price_kbb_suggest_retail: 13300, price_kbb_max_retail: 14000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 625,
                         vin: "WVWMP7AN8AE543519",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91702,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/24.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+VW+CC+Sport.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+VW+CC+Sport.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])



# 2011 Toyota Highlander
model = CarModel.find_by(styleId: 101351361)
car = model.cars.create!(mileage: 49500, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Black",
                         price: 23000, price_kbb_suggest_retail: 25280, price_kbb_max_retail: 26501,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1150,
                         vin: "5TDBK3EH1BS078323",
                         address: "", city: "Irvine", state: "CA", zipcode: 92604,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/25.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Toyota+Highlander+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Toyota+Highlander.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])


# 2011 Ford Fusion SE
model = CarModel.find_by(styleId: 101325678)
car = model.cars.create!(mileage: 51266, owner_id: 1, inspector_id: 2, int_color: "Beige", ext_color: "White",
                         price: 10000, price_kbb_suggest_retail: 12663, price_kbb_max_retail: 13600,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 500,
                         vin: "3FAHP0HA5BR211716",
                         address: "", city: "Irvine", state: "CA", zipcode: 92604,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/26.jpg"},
                         ])
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Ford+Fusion+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Ford+Fusion+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
m = car.car_model.dup
m.transmission = "6-speed Automatic with Overdrive"
m.save
car.car_model = m
car.save


Car.find_by(vin: "5J8TB18238A010645").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Acura+RDX/thumbnail.jpg")
Car.find_by(vin: "WDCGG5GB0AF491306").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Benz+GLK350/thumbnail.jpg")
Car.find_by(vin: "WBAPH5G51ANM35763").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+BMW/thumbnail.jpg")
Car.find_by(vin: "1G1ZC5EB8A4101120").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chevelot+Malibu+LT/thumbnail.jpg")
Car.find_by(vin: "5J6RE3H45BL047070").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+CRV/thumbnail.jpg")
Car.find_by(vin: "1HGCP3F85CA035575").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+EX-L/thumbnail.jpg")
Car.find_by(vin: "2HGFG3B84CH550040").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Civic/thumbnail.jpg")
Car.find_by(vin: "WMWSV3C56CTY19470").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+MINI+Cooper+S/thumbnail.jpg")
Car.find_by(vin: "1VWBP7A3XCC010833").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+passat/thumbnail.jpg")
Car.find_by(vin: "1C3CCBBBXDN542556").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chelyer/thumbnail.jpg")
Car.find_by(vin: "1FADP3K29DL337189").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Focus/thumbnail.jpg")
Car.find_by(vin: "JN1CV6AP4DM713188").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Infinity+G7+Final/thumbnail.jpg")
Car.find_by(vin: "3VW2K7AJ5DM358066").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+S/thumbnail.jpg")
Car.find_by(vin: "2HGFA1F5XBH527589").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Civic+LX/thumbnail.jpg")
Car.find_by(vin: "WAULFAFR1DA039572").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A5+Premium+Plus/thumbnail.jpg")
Car.find_by(vin: "1C3CCBCG3DN751955").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Chysler+200+Limited/thumbnail.jpg")
Car.find_by(vin: "WMWSY3C50DT594102").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Roadster+Cooper+S/thumbnail.jpg")
Car.find_by(vin: "4T4BF3EK3BR154735").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry/thumbnail.jpg")
Car.find_by(vin: "1HGCP2F38CA208987").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+Accord+LX/thumbnail.jpg")
Car.find_by(vin: "5J6RM3H50CL044235").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+L/thumbnail.jpg")
Car.find_by(vin: "5YFBURHE9EP031701").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Honda+CRV+EX/thumbnail.jpg")
Car.find_by(vin: "WDCGG5GB3AF402845").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+GLK+350/thumbnail.jpg")
Car.find_by(vin: "WBSDX9C55BE784120").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+M3+Convertible/thumbnail.jpg")
Car.find_by(vin: "WDDGF7HBXCA662149").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C63/thumbnail.jpg")
Car.find_by(vin: "WDDGF4HB2CA622565").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250/thumbnail.jpg")
Car.find_by(vin: "WBA3A5C59DF602426").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+BMW+328i/thumbnail.jpg")
Car.find_by(vin: "WMWMF73567TL91029").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2007+Mini+Cooper+S/thumbnail.jpg")
Car.find_by(vin: "3VWRG3AL0AM006390").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+New+Beetle/thumbnail.jpg")
Car.find_by(vin: "WAUKEAFM8DA025297").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A3/thumbnail.jpg")
Car.find_by(vin: "WBAKE5C5XCJ106501").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+BMW+328i/thumbnail.jpg")
Car.find_by(vin: "WAUEFAFL2DA080488").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Audi+A4/thumbnail.jpg")
Car.find_by(vin: "WMWSU3C57DT678073").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Cooper/thumbnail.jpg")
Car.find_by(vin: "WVWMP7AN9BE733346").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC+Sport/thumbnail.jpg")
Car.find_by(vin: "3VW2K7AJ0CM423923").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+Base-s/thumbnail.jpg")
Car.find_by(vin: "2HGFG1B85AH520417").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Honda+Civic+EX/thumbnail.jpg")
Car.find_by(vin: "WDDGF4HB0CA729372").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C250+Sports/thumbnail.jpg")
Car.find_by(vin: "2G1FC1E33C9142579").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Chevelot+Camero+LT+RS/thumbnail.jpg")
Car.find_by(vin: "3VWJP7AT7CM664800").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Beetle/thumbnail.jpg")
Car.find_by(vin: "4T1BF1FK7EU770648").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Camry+SE/thumbnail.jpg")
Car.find_by(vin: "3VWDP7AJ7CM315115").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Jetta+SE/thumbnail.jpg")
Car.find_by(vin: "3FA6P0G79DR367063").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Fusion/thumbnail.jpg")
Car.find_by(vin: "1VWAP7A34DC028922").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+PASSAT/thumbnail.jpg")
Car.find_by(vin: "WDDGF56X69R050849").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+Mercedes-Benz+C350/thumbnail.jpg")
Car.find_by(vin: "3VW2K7AJ1BM364802").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Jetta+S/thumbnail.jpg")
Car.find_by(vin: "5YFBURHE6EP051355").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Toyota+Corolla+LE/thumbnail.jpg")
Car.find_by(vin: "WMWMF33578TT65842").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Mini+Cooper/thumbnail.jpg")
Car.find_by(vin: "WVGAV7AX6BW560286").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+Tiguan+SE/thumbnail.jpg")
Car.find_by(vin: "1VWBP7A37CC004083").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+VW+Passat+SE/thumbnail.jpg")
Car.find_by(vin: "WDCGG8JB1DF941169").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Benz+GLK350/thumbnail.jpg")
Car.find_by(vin: "3CZRM3H36DG711693").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Honda+CRV/thumbnail.jpg")
Car.find_by(vin: "JN1CV6AR5AM450674").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Infiniti+G37/thumbnail.jpg")
Car.find_by(vin: "WVWMP7AN8AE543519").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+VW+CC+Sport/thumbnail.jpg")
Car.find_by(vin: "5TDBK3EH1BS078323").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Highlander/thumbnail.jpg")
Car.find_by(vin: "3FAHP0HA5BR211716").try(:update_attribute, :thumbnail, "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Ford+Fusion+SE/thumbnail.jpg")



# 2014 Ford Mustang
model = CarModel.find_by(styleId: 200465251)
car = model.cars.create!(mileage: 8000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 18000, price_kbb_suggest_retail: 21000, price_kbb_max_retail: 22300,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 900,
                         vin: "1ZVBP8AM3E5301888",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/26.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Mustang/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Ford+Mustang.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Ford+Mustang.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 12},
])
m = car.car_model.dup
m.transmission = "6-speed SelectShift Automatic"
m.save
car.car_model = m
car.save



# 2012 Mercedes-Benz ML350
model = CarModel.find_by(styleId: 101327700)
car = model.cars.create!(mileage: 26245, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 38000, price_kbb_suggest_retail: 38341, price_kbb_max_retail: 41048,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1900,
                         vin: "4JGDA5HB5CA002029",
                         address: "", city: "Irvine", state: "CA", zipcode: 92604,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/30.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Mercedes-Benz+ML350/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Mercedes-+Benz+ML350+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Mercedes-+Benz+ML350.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])


# 2009 BMW 328I
model = CarModel.find_by(styleId: 101082770)
car = model.cars.create!(mileage: 59467, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "White",
                         price: 15000, price_kbb_suggest_retail: 16994, price_kbb_max_retail: 18657,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 750,
                         vin: "WBAPH57539NM32813",
                         address: "", city: "Irvine", state: "CA", zipcode: 92604,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/28.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2009+BMW+328I/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2009+BMW+328+Inspection+report.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2009+BMW328+report.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])
m = car.car_model.dup
m.transmission = "6-speed Automatic with Overdrive & Steptronic"
m.save
car.car_model = m
car.save



# 2010 Toyota Camry
model = CarModel.find_by(styleId: 101147479)
car = model.cars.create!(mileage: 48000, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "White",
                         price: 12000, price_kbb_suggest_retail: 12600, price_kbb_max_retail: 13500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 600,
                         vin: "4T1BF3EK6AU559206",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/27.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Toyota+Camry/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Toyota+Camry+LE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Toyota+Camry+LE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])



# 2015 Toyota Camry SE
model = CarModel.find_by(styleId: 200711467)
car = model.cars.create!(mileage: 1800, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Gray",
                         price: 18500, price_kbb_suggest_retail: 21000, price_kbb_max_retail: 22360,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 925,
                         vin: "4T1BF1FK8FU105582",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/30.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2015+Toyota+Camry+SE/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2015+Toyota+Camry+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2015+Toyota+Camry+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
])


# 2010 Audi Q5 Premium plus
model = CarModel.find_by(styleId: 101187517)
car = model.cars.create!(mileage: 37000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Gray",
                         price: 25000, price_kbb_suggest_retail: 27221, price_kbb_max_retail: 28754,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1250,
                         vin: "WA1LKAFP2AA085799",
                         address: "", city: "Monterey Park", state: "CA", zipcode: 91754,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/31.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Audi+Q5+Premium+plus/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Audi+Q5+Premium+Plus.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Audi+Q5+Premium+Plus.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 19},
    {car_id: car.id, feature_id: 20},
])



# 2013 Juguar XF
model = CarModel.find_by(styleId: 200443908)
car = model.cars.create!(mileage: 32500, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "White",
                         price: 28000, price_kbb_suggest_retail: 34000, price_kbb_max_retail: 36000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1400.0,
                         vin: "SAJWA0ES4DPS67559",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/31.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/32.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/33.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Juguar+XF/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Juguar+XF.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Juguar+XF.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 14},
    {car_id: car.id, feature_id: 20},
    {car_id: car.id, feature_id: 21},
    {car_id: car.id, feature_id: 22},
    {car_id: car.id, feature_id: 24},
])


# 2008 Maserati GT
model = CarModel.find_by(styleId: 100986894)
car = model.cars.create!(mileage: 41000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 41000, price_kbb_suggest_retail: 46000, price_kbb_max_retail: 47800,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 2050.0,
                         vin: "ZAMGJ45A880041407",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/29.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2008+Maserati+Gt/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2008+Maserati+GranTurismo.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2008+Maserati+GranTurismo.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 14},
    {car_id: car.id, feature_id: 15},
    {car_id: car.id, feature_id: 20},
    {car_id: car.id, feature_id: 21},
    {car_id: car.id, feature_id: 22},
    {car_id: car.id, feature_id: 24},
])


# 2010 Bentley Continental Supersport
model = CarModel.find_by(styleId: 101162022)
car = model.cars.create!(mileage: 17000, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Silver",
                         price: 110000, price_kbb_suggest_retail: 115000, price_kbb_max_retail: 120000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 5500.0,
                         vin: "SCBCU8ZA9AC064181",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/30.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Bentley+Supersport/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Bentley+Continental+Supersport.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Bentley+Continental+Supersport.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 8},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 14},
    {car_id: car.id, feature_id: 20},
    {car_id: car.id, feature_id: 21},
    {car_id: car.id, feature_id: 22},
    {car_id: car.id, feature_id: 24},
])


# 2013 VW Tiguan S
model = CarModel.find_by(styleId: 200435812)
car = model.cars.create!(mileage: 52648, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Black",
                         price: 15700, price_kbb_suggest_retail: 15262, price_kbb_max_retail: 16118,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 785.0,
                         vin: "WVGAV7AX2DW539910",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/30.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+tiguan+S/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+VW+Tiguan+S.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+VW+Tiguan+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])


# 2011 Toyota Camry Se
model = CarModel.find_by(styleId: 101275057)
car = model.cars.create!(mileage: 52974, owner_id: 1, inspector_id: 1, int_color: "Silver", ext_color: "Silver",
                         price: 12000, price_kbb_suggest_retail: 13883, price_kbb_max_retail: 14779,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 600.0,
                         vin: "4T1BF3EK1BU621161",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/24.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Toyota+Camry+SE/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Toyota+Camry+Se.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Toyota+Camry+SE.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])



# 2010 Chrysler Sebring Touring
model = CarModel.find_by(styleId: 101225861)
car = model.cars.create!(mileage: 60470, owner_id: 1, inspector_id: 1, int_color: "Black", ext_color: "Green",
                         price: 7300, price_kbb_suggest_retail: 10213, price_kbb_max_retail: 10501,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 365.0,
                         vin: "1C3BC5ED3AN199885",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/30.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Chrysler+Sebring/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Chrysler+Sebring+Touring.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Chrysler+Sebring+Touring.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])



# 2011 Honda Accord LX-P
model = CarModel.find_by(styleId: 101356474)
car = model.cars.create!(mileage: 41948, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "Black",
                         price: 12600, price_kbb_suggest_retail: 13484, price_kbb_max_retail: 14209,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 630,
                         vin: "1HGCP2F44BA087477",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90007,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/28.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+LX-P/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Honda+Accord+LX-P.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Honda+Accord+LX-P.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 20},
    {car_id: car.id, feature_id: 21},
])



# 2011 BMW 328i
model = CarModel.find_by(styleId: 101288737)
car = model.cars.create!(mileage: 57000, owner_id: 1, inspector_id: 2, int_color: "Beige", ext_color: "White",
                         price: 16899, price_kbb_suggest_retail: 17720, price_kbb_max_retail: 19215,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 844.95,
                         vin: "WBAPH5C51BA442145",
                         address: "", city: "Woodland Hills", state: "CA", zipcode: 91367,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/28.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+BMW+328i/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+BMW+328i.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+BMW+328i.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])
m = car.car_model.dup
m.transmission = "6-speed Automatic with Overdrive & Steptronic"
m.save
car.car_model = m
car.save


# 2011 Nissan Sentra
model = CarModel.find_by(styleId: 101357674)
car = model.cars.create!(mileage: 41000, owner_id: 1, inspector_id: 1, int_color: "Gray", ext_color: "Silver",
                         price: 8900, price_kbb_suggest_retail: 10738, price_kbb_max_retail: 11753,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 445.0,
                         vin: "3N1AB6APXBL625883",
                         address: "", city: "El Monte", state: "CA", zipcode: 91733,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/31.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan/32.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Nissan+Sentra/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Nissan+Sentra+Base.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Nissan+Sentra.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])



# 2014 Ford Escape
model = CarModel.find_by(styleId: 200466871)
car = model.cars.create!(mileage: 18427, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 16000, price_kbb_suggest_retail: 16934, price_kbb_max_retail: 18764,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 800.0,
                         vin: "1FMCU0F77EUD80665",
                         address: "", city: "Glendale", state: "CA", zipcode: 91502,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/24.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Ford+Escape+S/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Ford+Escape+S+Inspection.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Ford+Escape+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])



# 2011 VW CC Sport
model = CarModel.find_by(styleId: 101336021)
car = model.cars.create!(mileage: 65483, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Silver Gray",
                         price: 11000, price_kbb_suggest_retail: 12199, price_kbb_max_retail: 13771,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 550.0,
                         vin: "WVWMP7AN0BE732201",
                         address: "", city: "San Gabriel", state: "CA", zipcode: 91776,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/31.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/32.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/33.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/34.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/35.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/36.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/37.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/38.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/39.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+VW+CC/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+VW+CC+Sport.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+VW+CC+Sport.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 6},
    {car_id: car.id, feature_id: 12},
    {car_id: car.id, feature_id: 21},
])



# 2011 Honda Accord Coupe EX
model = CarModel.find_by(styleId: 101356502)
car = model.cars.create!(mileage: 33000, owner_id: 1, inspector_id: 1, int_color: "Beige", ext_color: "White",
                         price: 14500, price_kbb_suggest_retail: 15200, price_kbb_max_retail: 16432,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 725.0,
                         vin: "1HGCS1B75BA011405",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/26.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2011+Honda+Accord+Coupe+Ex/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2011+Honda+Accord+Coupe+EX.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2011+Honda+Accord+Coupe+EX.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 2},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 15},
    {car_id: car.id, feature_id: 22},
    {car_id: car.id, feature_id: 23},
    {car_id: car.id, feature_id: 25},
])


# 2010 Porsche Panamera S
model = CarModel.find_by(styleId: 101158930)
car = model.cars.create!(mileage: 38000, owner_id: 1, inspector_id: 1, int_color: "Brown", ext_color: "White",
                         price: 53000, price_kbb_suggest_retail: 54514, price_kbb_max_retail: 56140,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 2650.0,
                         vin: "WP0AB2A70AL060589",
                         address: "", city: "Monrovia", state: "CA", zipcode: 91016,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/22.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/23.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/24.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/25.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/26.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/27.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/28.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/29.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/30.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/31.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/32.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/33.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/34.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/35.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/36.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/37.jpg"},
                         ])
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2010+Porsche+Panamera+S/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2010+Porsche+Panamera+S.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2010+Porsche+Panamera+S.pdf"
car.save
CarFeatureRelation.create!([
    {car_id: car.id, feature_id: 1},
    {car_id: car.id, feature_id: 3},
    {car_id: car.id, feature_id: 4},
    {car_id: car.id, feature_id: 5},
    {car_id: car.id, feature_id: 7},
    {car_id: car.id, feature_id: 11},
    {car_id: car.id, feature_id: 13},
    {car_id: car.id, feature_id: 15},
    {car_id: car.id, feature_id: 20},
    {car_id: car.id, feature_id: 22},
    {car_id: car.id, feature_id: 24},
    {car_id: car.id, feature_id: 25},
    {car_id: car.id, feature_id: 26},
    {car_id: car.id, feature_id: 27},
    {car_id: car.id, feature_id: 28},
    {car_id: car.id, feature_id: 29},
    {car_id: car.id, feature_id: 30},
    {car_id: car.id, feature_id: 31},
    {car_id: car.id, feature_id: 32},
])



# 2013 Mini Paceman
model = CarModel.find_by(styleId: 101407244)
car = model.cars.create!(mileage: 21000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Red",
                         price: 16500, price_kbb_suggest_retail: 17800, price_kbb_max_retail: 19400,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 825.0,
                         vin: "WMWSS1C50DWN94038",
                         address: "", city: "Los Angeles", state: "CA", zipcode: 90066,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+Mini+Paceman.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+Mini+Paceman.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 25/Hwy 30/Comb 27 MPG","Max Seating":"5","Doors":"2","Engine":"4-Cyl, 1.6 Liter","Drivetrain":"FWD","Transmission":"Auto, 6-Spd Steptronic","EPA Class":"Compact Cars","Body Style":"Hatchback","Country of Origin":"United Kingdom","Country of Assembly":"United Kingdom"},"tech":{"Engine":"4-Cyl, 1.6 Liter","Horsepower":"121 @ 6000 RPM","Torque":"118 @ 4250 RPM","Fuel Economy":"City 25/Hwy 30/Comb 27 MPG","Bore x Stroke":"3.00 x 3.40","Compression Ratio":"11.0","Fuel Type":"Gas","Fuel Induction":"-","Valve Train":"Dual Overhead Cam","Valves Per Cylinder":"4","Total Number Valves":"16","Transmission":"Auto, 6-Spd Steptronic","Drivetrain":"FWD","Transfer Case":"-","Fuel Capacity":"12.4 gallons","Wheel Base":"102.2 inches","Overall Length":"161.7 inches","Width with Mirrors":"78.6 inches","Width without Mirrors":"70.4 inches","Height":"61.5 inches","Curb Weight":"2940 lbs.","Tires / Wheel Size":"P205/55R17","Rear Tires / Wheel Size":"-","Turning Diameter":"38.1 feet","Standard Axle Ratio":"4.72","Minimum Ground Clearance":"-","Maximum Ground Clearance":"-","Maximum GVWR":"3850 lbs.","Maximum Towing":"Not Recommended","Payload Base Capacity":"850 lbs.","Head Room: Front":"39.9 inches","Head Room: Rear":"37.5 inches","Leg Room: Front":"-","Leg Room: Rear":"-","Shoulder Room: Front":"52.6 inches","Shoulder Room: Rear":"52.0 inches","EPA Passenger":"-","EPA Trunk or Cargo":"38.1 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerFront Head CurtainFront KneeFront Side","Child Door Locks":"Not available","Engine Immobilizer":"Standard","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"4 years or 50000 miles","Powertrain":"4 years or 50000 miles","Corrosion/Rust Thru":"12 years with Unlimited miles","Roadside Assistance Program":"4 years with Unlimited miles","Hybrid/Electric Components":"-"},"options":{"Engine":["4-Cyl, 1.6 Liter "],"Transmission":["Auto, 6-Spd Steptronic "],"Drivetrain":["FWD "],"Braking and Traction":["Hill Start Assist Control ","Dynamic Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Entry ","Keyless Start ","Air Conditioning ","Power Windows ","Cruise Control "],"Steering":["Power Steering ","Tilt & Telescoping Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","MP3 (Single Disc) ","Bluetooth Wireless "],"Safety and Security":["Dual Air Bags ","Side Air Bags ","Head Curtain Air Bags "],"Exterior":["Rear Spoiler "],"Wheels and Tires":["Alloy Wheels "]}}'
m = car.car_model.dup
m.transmission = "Auto, 6-Spd Steptronic"
m.save
car.car_model = m
car.save
car.car_pictures.create!([
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/01.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/02.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/03.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/04.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/05.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/06.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/07.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/08.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/09.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/10.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/11.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/12.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/13.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/14.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/15.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/16.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/17.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/18.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/19.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/20.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/21.jpg"},
                            {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+Mini+Paceman/22.jpg"},
                         ])


# 2013 VW Jetta SE
model = CarModel.find_by(styleId: 200434632)
car = model.cars.create!(mileage: 26500, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Gray",
                         price: 12500, price_kbb_suggest_retail: 13890, price_kbb_max_retail: 15110,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 625.0,
                         vin: "3VWDP7AJ3DM362708",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2013+VW+Jetta+SE.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2013+VW+Jetta+SE.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 24/Hwy 31/Comb 26 MPG","Max Seating":"5","Doors":"4","Engine":"5-Cyl, 2.5 Liter","Drivetrain":"FWD","Transmission":"Auto 6-Spd w/Tptrnc & Spt","EPA Class":"Compact Cars","Body Style":"Sedan","Country of Origin":"Germany","Country of Assembly":"Mexico"},"tech":{"Engine":"5-Cyl, 2.5 Liter","Horsepower":"170 @ 5700 RPM","Torque":"177 @ 4250 RPM","Fuel Economy":"City 24/Hwy 31/Comb 26 MPG","Bore x Stroke":"3.25 x 3.65","Compression Ratio":"9.5","Fuel Type":"Gas","Fuel Induction":"Multi Fuel Injection","Valve Train":"Dual Overhead Cam","Valves Per Cylinder":"4","Total Number Valves":"20","Transmission":"Auto 6-Spd w/Tptrnc & Spt","Drivetrain":"FWD","Transfer Case":"-","Fuel Capacity":"14.5 gallons","Wheel Base":"104.4 inches","Overall Length":"182.2 inches","Width with Mirrors":"70.0 inches","Width without Mirrors":"-","Height":"57.2 inches","Curb Weight":"3083 lbs.","Tires / Wheel Size":"P205/55HR16","Rear Tires / Wheel Size":"-","Turning Diameter":"36.4 feet","Standard Axle Ratio":"3.50","Minimum Ground Clearance":"5.5 inches","Maximum Ground Clearance":"-","Maximum GVWR":"-","Maximum Towing":"-","Payload Base Capacity":"1072 lbs.","Head Room: Front":"38.2 inches","Head Room: Rear":"37.1 inches","Leg Room: Front":"41.2 inches","Leg Room: Rear":"38.1 inches","Shoulder Room: Front":"55.2 inches","Shoulder Room: Rear":"53.6 inches","EPA Passenger":"94.1 cu.ft.","EPA Trunk or Cargo":"15.5 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerFront Head CurtainFront SideRear Head Curtain","Child Door Locks":"Standard","Engine Immobilizer":"Standard","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"3 years or 36000 miles","Powertrain":"5 years or 60000 miles","Corrosion/Rust Thru":"12 years with Unlimited miles","Roadside Assistance Program":"3 years or 36000 miles","Hybrid/Electric Components":"-"},"options":{"Engine":["5-Cyl, 2.5 Liter "],"Transmission":["Auto 6-Spd w/Tptrnc & Spt "],"Drivetrain":["FWD "],"Braking and Traction":["Traction Control ","Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Entry ","Air Conditioning ","Power Windows ","Power Door Locks ","Cruise Control "],"Steering":["Power Steering ","Tilt & Telescoping Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","MP3 (Single Disc) "],"Safety and Security":["Dual Air Bags ","Side Air Bags ","F&R Head Curtain Air Bags "],"Seats":["Dual Power Seats "],"Lighting":["Daytime Running Lights "],"Wheels and Tires":["Steel Wheels "]}}'
car.save
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2013+VW+Jetta+SE/24.jpg"},
                         ])


# 2014 Mercedes- Benz Cla C250
model = CarModel.find_by(styleId: 101410564)
car = model.cars.create!(mileage: 27600, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Red",
                         price: 28500, price_kbb_suggest_retail: 29080, price_kbb_max_retail: 31314,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1425.0,
                         vin: "WDDSJ4EB9EN060219",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Benz+Cla+C250.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Benz+Cla+c250.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 26/Hwy 38/Comb 30 MPG","Max Seating":"5","Doors":"4","Engine":"4-Cyl, Turbo, 2.0 Liter","Drivetrain":"FWD","Transmission":"Auto, 7-Spd Dble Clutch","EPA Class":"Compact Cars","Body Style":"Coupe","Country of Origin":"Germany","Country of Assembly":"Germany"},"tech":{"Engine":"4-Cyl, Turbo, 2.0 Liter","Horsepower":"208 @ 5500 RPM","Torque":"258 @ 1250 RPM","Fuel Economy":"City 26/Hwy 38/Comb 30 MPG","Bore x Stroke":"3.27 x 3.62","Compression Ratio":"9.8","Fuel Type":"Gas","Fuel Induction":"Direct Injection","Valve Train":"-","Valves Per Cylinder":"4","Total Number Valves":"16","Transmission":"Auto, 7-Spd Dble Clutch","Drivetrain":"FWD","Transfer Case":"-","Fuel Capacity":"14.8 gallons","Wheel Base":"106.3 inches","Overall Length":"182.3 inches","Width with Mirrors":"80.0 inches","Width without Mirrors":"70.0 inches","Height":"56.6 inches","Curb Weight":"3262 lbs.","Tires / Wheel Size":"P225/45R17","Rear Tires / Wheel Size":"-","Turning Diameter":"36.0 feet","Standard Axle Ratio":"4.13","Minimum Ground Clearance":"3.9 inches","Maximum Ground Clearance":"-","Maximum GVWR":"-","Maximum Towing":"-","Payload Base Capacity":"-","Head Room: Front":"38.2 inches","Head Room: Rear":"35.4 inches","Leg Room: Front":"40.2 inches","Leg Room: Rear":"27.1 inches","Shoulder Room: Front":"56.0 inches","Shoulder Room: Rear":"53.2 inches","EPA Passenger":"-","EPA Trunk or Cargo":"13.1 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerDriver KneeFront Head CurtainFront KneeFront SideRear Head Curtain","Child Door Locks":"-","Engine Immobilizer":"Standard","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"4 years or 50000 miles","Powertrain":"4 years or 50000 miles","Corrosion/Rust Thru":"4 years or 50000 miles","Roadside Assistance Program":"Unlimited years with Unlimited miles","Hybrid/Electric Components":"-"},"options":{"Engine":["4-Cyl, Turbo, 2.0 Liter "],"Transmission":["Auto, 7-Spd Dble Clutch "],"Drivetrain":["FWD "],"Braking and Traction":["Hill Start Assist Control ","Traction Control ","Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Entry ","Air Conditioning ","Power Windows ","Power Door Locks "],"Steering":["Power Steering ","Tilt & Telescoping Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","MP3 (Single Disc) ","Bluetooth Wireless ","mbrace2 "],"Safety and Security":["Dual Air Bags ","Side Air Bags ","F&R Head Curtain Air Bags "],"Seats":["Dual Power Seats ","Leather "],"Lighting":["Daytime Running Lights ","Bi-HID Headlamps "],"Wheels and Tires":["Alloy Wheels "]}}'
car.save
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/24.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/25.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/26.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/27.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/28.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/29.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+CLA+250/30.jpg"},
                         ])


# 2012 Mercedes- Benz C350 Coupe
model = CarModel.find_by(styleId: 101397772)
car = model.cars.create!(mileage: 48700, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 27900, price_kbb_suggest_retail: 29900, price_kbb_max_retail: 31000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1395.0,
                         vin: "WDDGJ5HB6CF778731",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2012+Benz+C350+Coupe.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2012+Benz+C350+Coupe.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 19/Hwy 28/Comb 22 MPG","Max Seating":"4","Doors":"2","Engine":"V6, 3.5 Liter","Drivetrain":"RWD","Transmission":"Auto, 7-Spd Touch Shift","EPA Class":"Sub Compact Cars","Body Style":"Coupe","Country of Origin":"Germany","Country of Assembly":"Germany"},"tech":{"Engine":"V6, 3.5 Liter","Horsepower":"302 @ 6500 RPM","Torque":"273 @ 3500 RPM","Fuel Economy":"City 19/Hwy 28/Comb 22 MPG","Bore x Stroke":"3.66 x 3.39","Compression Ratio":"12.2","Fuel Type":"Gas","Fuel Induction":"Sequential Fuel Injection","Valve Train":"Dual Overhead Cam","Valves Per Cylinder":"4","Total Number Valves":"24","Transmission":"Auto, 7-Spd Touch Shift","Drivetrain":"RWD","Transfer Case":"-","Fuel Capacity":"17.4 gallons","Wheel Base":"108.7 inches","Overall Length":"180.7 inches","Width with Mirrors":"69.7 inches","Width without Mirrors":"-","Height":"54.8 inches","Curb Weight":"3562 lbs.","Tires / Wheel Size":"P225/45R17","Rear Tires / Wheel Size":"P245/40R17","Turning Diameter":"35.6 feet","Standard Axle Ratio":"2.82","Minimum Ground Clearance":"-","Maximum Ground Clearance":"-","Maximum GVWR":"-","Maximum Towing":"Not Recommended","Payload Base Capacity":"-","Head Room: Front":"37.0 inches","Head Room: Rear":"35.5 inches","Leg Room: Front":"42.0 inches","Leg Room: Rear":"-","Shoulder Room: Front":"54.0 inches","Shoulder Room: Rear":"50.3 inches","EPA Passenger":"-","EPA Trunk or Cargo":"11.7 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerRear SideDriver KneeFront Head CurtainFront SideRear Head Curtain","Child Door Locks":"Standard","Engine Immobilizer":"Standard","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"4 years or 50000 miles","Powertrain":"4 years or 50000 miles","Corrosion/Rust Thru":"4 years or 50000 miles","Roadside Assistance Program":"Unlimited years with Unlimited miles","Hybrid/Electric Components":"-"},"options":{"Engine":["V6, 3.5 Liter "],"Transmission":["Auto, 7-Spd Touch Shift "],"Drivetrain":["RWD "],"Accessory Packages":["Premium Pkg 1 "],"Braking and Traction":["Traction Control ","Electronic Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Entry ","Air Conditioning ","Power Windows ","Power Door Locks ","Cruise Control "],"Steering":["Power Steering ","Tilt & Telescoping Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","MP3 (Single Disc) ","SiriusXM Satellite ","Bluetooth Wireless "],"Safety and Security":["Parktronic ","Backup Camera ","Dual Air Bags ","F&R Side Air Bags ","F&R Head Curtain Air Bags "],"Seats":["Heated Seats ","Dual Power Seats ","Leather "],"Roof and Glass":["Panorama Roof "],"Lighting":["Daytime Running Lights "],"Wheels and Tires":["Alloy Wheels "]}}'
car.save
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/24.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/25.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/26.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/27.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/28.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/29.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/30.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2012+Benz+C350+Coupe/31.jpg"},
                         ])


# 2014 Mercedes- Benz C250 Sports
model = CarModel.find_by(styleId: 200482167)
car = model.cars.create!(mileage: 13000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "White",
                         price: 24500, price_kbb_suggest_retail: 26000, price_kbb_max_retail: 27500,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 1225.0,
                         vin: "WDDGF4HBXEA952702",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Benz+C250+Sports.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Benz+C250+Sports.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 22/Hwy 31/Comb 25 MPG","Max Seating":"5","Doors":"4","Engine":"4-Cyl, Turbo, 1.8 Liter","Drivetrain":"RWD","Transmission":"Auto, 7-Spd Touch Shift","EPA Class":"Compact Cars","Body Style":"Sedan","Country of Origin":"Germany","Country of Assembly":"Germany"},"tech":{"Engine":"4-Cyl, Turbo, 1.8 Liter","Horsepower":"201 @ 5500 RPM","Torque":"229 @ 2200 RPM","Fuel Economy":"City 22/Hwy 31/Comb 25 MPG","Bore x Stroke":"3.23 x 3.35","Compression Ratio":"9.3","Fuel Type":"Gas","Fuel Induction":"Direct Injection","Valve Train":"Dual Overhead Cam","Valves Per Cylinder":"4","Total Number Valves":"16","Transmission":"Auto, 7-Spd Touch Shift","Drivetrain":"RWD","Transfer Case":"-","Fuel Capacity":"17.4 gallons","Wheel Base":"108.7 inches","Overall Length":"180.8 inches","Width with Mirrors":"79.1 inches","Width without Mirrors":"69.7 inches","Height":"56.3 inches","Curb Weight":"3428 lbs.","Tires / Wheel Size":"P225/45R17","Rear Tires / Wheel Size":"P245/40R17","Turning Diameter":"35.3 feet","Standard Axle Ratio":"-","Minimum Ground Clearance":"-","Maximum Ground Clearance":"-","Maximum GVWR":"-","Maximum Towing":"Not Recommended","Payload Base Capacity":"-","Head Room: Front":"37.1 inches","Head Room: Rear":"36.9 inches","Leg Room: Front":"41.7 inches","Leg Room: Rear":"33.4 inches","Shoulder Room: Front":"54.7 inches","Shoulder Room: Rear":"55.0 inches","EPA Passenger":"-","EPA Trunk or Cargo":"12.4 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerDriver KneeFront Head CurtainFront SideRear Head Curtain","Child Door Locks":"Standard","Engine Immobilizer":"Standard","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"4 years or 50000 miles","Powertrain":"4 years or 50000 miles","Corrosion/Rust Thru":"4 years or 50000 miles","Roadside Assistance Program":"Unlimited years with Unlimited miles","Hybrid/Electric Components":"-"},"options":{"Engine":["4-Cyl, Turbo, 1.8 Liter "],"Transmission":["Auto, 7-Spd Touch Shift "],"Drivetrain":["RWD "],"Braking and Traction":["Traction Control ","Electronic Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Entry ","Air Conditioning ","Power Windows ","Power Door Locks ","Cruise Control "],"Steering":["Tilt & Telescoping Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","CD (Single Disc) ","Bluetooth Wireless ","mbrace2 "],"Safety and Security":["Dual Air Bags ","Side Air Bags ","F&R Head Curtain Air Bags "],"Seats":["Dual Power Seats ","Leather "],"Roof and Glass":["Moon Roof "],"Lighting":["Daytime Running Lights "],"Wheels and Tires":["Alloy Wheels "]}}'
car.save
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/24.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/25.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/26.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Benz+C250/27.jpg"},
                         ])


# 2014 Maserati Ghibli S Q4
model = CarModel.find_by(styleId: 200477737)
car = model.cars.create!(mileage: 13000, owner_id: 1, inspector_id: 2, int_color: "Black", ext_color: "Black",
                         price: 57500, price_kbb_suggest_retail: 58500, price_kbb_max_retail: 60000,
                         price_registration_fee: 42, price_document_fee: 50, price_service_fee: 2875.0,
                         vin: "ZAM57XSA0E1087637",
                         address: "", city: "Alhambra", state: "CA", zipcode: 91801,
                         sold: false, interior_color_id: 1, exterior_color_id: 3, status: 3)
car.thumbnail = "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/thumbnail.jpg"
car.checklistpdf = "https://s3-us-west-2.amazonaws.com/tplchecklist/2014+Maserati+Ghibli+.pdf"
car.autocheck = "https://s3-us-west-1.amazonaws.com/tplreports/2014+Maserati+Ghibli.pdf"
car.detail = '{"highlights":{"Fuel Economy":"City 15/Hwy 25/Comb 18 MPG","Max Seating":"5","Doors":"4","Engine":"V6, Twin Turbo, 3.0 Liter","Drivetrain":"AWD","Transmission":"Auto, 8-Spd w/MM","EPA Class":"Large Cars","Body Style":"Sedan","Country of Origin":"Italy","Country of Assembly":"Italy"},"tech":{"Engine":"V6, Twin Turbo, 3.0 Liter","Horsepower":"404 @ 5500 RPM","Torque":"406 @ 1750 RPM","Fuel Economy":"City 15/Hwy 25/Comb 18 MPG","Bore x Stroke":"-","Compression Ratio":"9.7","Fuel Type":"Gas","Fuel Induction":"Direct Injection","Valve Train":"-","Valves Per Cylinder":"-","Total Number Valves":"-","Transmission":"Auto, 8-Spd w/MM","Drivetrain":"AWD","Transfer Case":"-","Fuel Capacity":"21.1 gallons","Wheel Base":"118.0 inches","Overall Length":"195.7 inches","Width with Mirrors":"82.7 inches","Width without Mirrors":"76.6 inches","Height":"57.5 inches","Curb Weight":"4123 lbs.","Tires / Wheel Size":"P245/45R19","Rear Tires / Wheel Size":"P275/40R19","Turning Diameter":"-","Standard Axle Ratio":"-","Minimum Ground Clearance":"-","Maximum Ground Clearance":"-","Maximum GVWR":"-","Maximum Towing":"-","Payload Base Capacity":"-","Head Room: Front":"-","Head Room: Rear":"-","Leg Room: Front":"-","Leg Room: Rear":"-","Shoulder Room: Front":"-","Shoulder Room: Rear":"-","EPA Passenger":"-","EPA Trunk or Cargo":"18.0 cu.ft.","EPA Total Interior":"-","Truck Bed Volume":"-"},"safety":{"Standard Airbag":"DriverPassengerDriver KneeFront Head CurtainFront SideRear Head Curtain","Child Door Locks":"-","Engine Immobilizer":"Not Available","Traction Control":"-","Communication System":"-"},"warranty":{"Basic":"4 years or 50000 miles","Powertrain":"-","Corrosion/Rust Thru":"-","Roadside Assistance Program":"4 years or 50000 miles","Hybrid/Electric Components":"-"},"options":{"Engine":["V6, Twin Turbo, 3.0 Liter "],"Transmission":["Auto, 8-Spd w/MM "],"Drivetrain":["AWD "],"Braking and Traction":["Hill Start Assist ","Stability Control ","ABS (4-Wheel) "],"Comfort and Convenience":["Keyless Start ","Air Conditioning ","Power Windows ","Power Door Locks ","Active Cruise Control "],"Steering":["Power Steering ","Tilt Wheel "],"Entertainment and Instrumentation":["AM/FM Stereo ","SiriusXM Satellite ","Navigation System ","Bluetooth Wireless "],"Safety and Security":["Parking Sensors ","Backup Camera ","Dual Air Bags ","Side Air Bags ","F&R Head Curtain Air Bags "],"Seats":["Dual Power Seats ","Leather "],"Roof and Glass":["Sun Roof (Sliding) "],"Lighting":["Daytime Running Lights ","Bi-HID Headlamps "],"Wheels and Tires":["Premium Wheels 19\"+ "]}}'
car.save
car.car_pictures.create!([
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/01.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/02.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/03.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/04.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/05.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/06.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/07.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/08.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/09.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/10.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/11.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/12.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/13.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/14.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/15.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/16.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/17.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/18.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/19.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/20.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/21.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/22.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/23.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/24.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/25.jpg"},
                             {remote_picture_url: "https://s3-us-west-1.amazonaws.com/tplcarpics/2014+Maserati+Ghibli+S+Q4/26.jpg"},
                         ])


