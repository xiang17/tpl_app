require 'api_constraints'

Rails.application.routes.draw do
  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  devise_for :experts, controllers: {
    # registrations: 'experts/registrations',
    sessions: 'experts/sessions',
  }
  devise_for :users, controllers: {
    # registrations: 'users/registrations',
    omniauth_callbacks: "users/omniauth_callbacks"
  }

  scope "(:locale)", locale: /en|zh/ do
    resources :cars do
      collection do
        get   'models'
        get   'bodystyles'
        get   'styletrims'
        get   'fetch'
        get   'checklistdefinition'
        get   'car_detail'
	get   'car_love'
      end
      member do
        get   'checklist'
        post  'checklist'
        get   'craigslist'
      end
      resources :car_pictures
    end
    match '/search/cars', to: 'cars#index', via: 'post'
    match '/search/cars', to: 'cars#index', via: 'get'


    # Routes for handling the operation on user's cars
    resources :users do
      member do
        get :cars
        get :appointments
        get :sell_transactions
        get :buy_transactions
        get :appointment_list
      end
      collection do
        post :signin
        post :signup
      end
    end


    resources :charges do
      collection do
        post :charge
      end
    end

    resources :easy_transfers do
      member do
        post  :confirm
      end
    end

    # resources :experts do
    resources :experts, only: [:index, :show, :edit, :update] do
      member do
        get   :editAvailableTime
        post  :updateAvailableTime
        get   :scheduleAppointments
        post  :scheduleAppointments
        get   :reports
        get   :appointments
      end
    end

    resources :sell_requests do
      collection do
        post :new_user
        post :create_appointment
        get :get_all_price
        get :get_vininfo
        get :available_time_windows
      end
    end

    resources :appointments do
      collection do
        get :available_time_windows
      end
    end

    # Different root paths
    match '/faq', to: 'static_pages#faq', via: 'get'
    match '/terms_policy', to: 'static_pages#terms_policy', via: 'get'


    #car model
    resource :car_models do
      get 'models', on: :collection
      get 'years', on: :collection
      get 'stylenames', on: :collection
      get 'get_make', on: :collection
      get 'get_model', on: :collection
      get 'get_body', on: :collection
      get 'get_trim', on: :collection
      get 'get_drivetrain', on: :collection
      get 'get_transmission', on: :collection
    end

    resources :car_transactions
    resources :car_pictures

    match '/driver_license' => 'driver_license#index', via: 'get'
    match '/driver_license/game' => 'driver_license#game', via: 'get'
    match '/driver_license/test' => 'driver_license#test', via: 'get'
    match '/driver_license/practice' => 'driver_license#practice', via: 'get'
    match '/driver_license/newgame' => 'driver_license#newgame', via: 'get'
    match '/driver_license/:id' => 'driver_license#show', via: 'get'

  end

  #Api definition
  namespace :api, defaults: {format: :json} do
    scope module: :v1 , constraints: ApiConstraints.new(version: 1, default: true) do
      #Our resources here
      # get 'api_tokens', to: :index
      # get 'api_tokens/show', to: :show
      get 'expertapi/get_token' => 'expertapi#get_token'
      get 'expertapi/get_expert_info' => 'expertapi#get_expert_info'
      get 'expertapi/get_appointments' => 'expertapi#get_appointments'
      get 'expertapi/get_user_info' => 'expertapi#get_user_info'
      get 'expertapi/get_time_window' => 'expertapi#get_time_window'

    end
  end

  root 'static_pages#home'

  get '/:locale' => 'static_pages#home'

end
