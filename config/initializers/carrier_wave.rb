CarrierWave.configure do |config|
  config.fog_credentials = {
    provider:              'AWS',                        
    aws_access_key_id:     ENV['access_key_id'],                        
    aws_secret_access_key: ENV['secret_access_key'],                        
    region:                ENV['s3_region'],
  }
  config.fog_directory  = ENV['s3_bucket']
  config.fog_attributes = { 'Cache-Control' => "max-age=#{365.day.to_i}" } 
end
