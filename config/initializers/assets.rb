# Be sure to restart your server when you modify this file.

# Version of your assets, change this if you want to expire all your assets.
Rails.application.config.assets.version = '1.0'

Rails.application.config.assets.precompile += %w( jquery.ui.addresspicker.js )

Rails.application.config.assets.precompile += %w(vendor/bootstrap/bootstrap.css)
Rails.application.config.assets.precompile += %w(vendor/fontawesome/css/font-awesome.css)
Rails.application.config.assets.precompile += %w(vendor/owlcarousel/owl.carousel.min.css)
Rails.application.config.assets.precompile += %w(vendor/owlcarousel/owl.theme.default.min.css)
Rails.application.config.assets.precompile += %w(vendor/magnific-popup/magnific-popup.css)

Rails.application.config.assets.precompile += %w(css/theme.css)
Rails.application.config.assets.precompile += %w(css/theme-elements.css)
Rails.application.config.assets.precompile += %w(css/theme-blog.css)
Rails.application.config.assets.precompile += %w(css/theme-shop.css)
Rails.application.config.assets.precompile += %w(css/theme-animate.css)

Rails.application.config.assets.precompile += %w(vendor/rs-plugin/css/settings.css)
Rails.application.config.assets.precompile += %w(vendor/circle-flip-slideshow/css/component.css)
Rails.application.config.assets.precompile += %w(css/skins/default.css)
Rails.application.config.assets.precompile += %w(css/custom.css)
Rails.application.config.assets.precompile += %w(vendor/modernizr/modernizr.js)

Rails.application.config.assets.precompile += %w(vendor/jquery/jquery.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.appear/jquery.appear.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.easing/jquery.easing.js)
Rails.application.config.assets.precompile += %w(vendor/jquery-cookie/jquery-cookie.js)
Rails.application.config.assets.precompile += %w(vendor/bootstrap/bootstrap.js)
Rails.application.config.assets.precompile += %w(vendor/common/common.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.validation/jquery.validation.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.stellar/jquery.stellar.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js)
Rails.application.config.assets.precompile += %w(vendor/jquery.gmap/jquery.gmap.js)
Rails.application.config.assets.precompile += %w(vendor/isotope/jquery.isotope.js)
Rails.application.config.assets.precompile += %w(vendor/owlcarousel/owl.carousel.js)
Rails.application.config.assets.precompile += %w(vendor/jflickrfeed/jflickrfeed.js)
Rails.application.config.assets.precompile += %w(vendor/magnific-popup/jquery.magnific-popup.js)
Rails.application.config.assets.precompile += %w(vendor/vide/vide.js)
Rails.application.config.assets.precompile += %w(js/theme.js)
Rails.application.config.assets.precompile += %w(vendor/rs-plugin/js/jquery.themepunch.tools.min.js)
Rails.application.config.assets.precompile += %w(vendor/rs-plugin/js/jquery.themepunch.revolution.min.js)
Rails.application.config.assets.precompile += %w(vendor/circle-flip-slideshow/js/jquery.flipshow.js)
Rails.application.config.assets.precompile += %w(js/views/view.home.js)
Rails.application.config.assets.precompile += %w(js/views/view.contact.js)
Rails.application.config.assets.precompile += %w(js/custom.js)
Rails.application.config.assets.precompile += %w(js/theme.init.js)

Rails.application.config.assets.precompile += %w(images/*.png)
Rails.application.config.assets.precompile += %w(images/*.jpg)
Rails.application.config.assets.precompile += %w( bootstrap.icon-large.min.css )

Rails.application.config.assets.precompile += %w( css/swiper.css )
Rails.application.config.assets.precompile += %w( swiper/swiper.min.js )
# Add additional assets to the asset load path
# Rails.application.config.assets.paths << Emoji.images_path

# Precompile additional assets.
# application.js, application.css, and all non-JS/CSS in app/assets folder are already added.
# Rails.application.config.assets.precompile += %w( search.js )
