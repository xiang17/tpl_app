require 'test_helper'

class EasyTransfersControllerTest < ActionController::TestCase
  setup do
    @easy_transfer = easy_transfers(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:easy_transfers)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create easy_transfer" do
    assert_difference('EasyTransfer.count') do
      post :create, easy_transfer: { actual_payer_email: @easy_transfer.actual_payer_email, actual_payer_mobile: @easy_transfer.actual_payer_mobile, actual_payer_name: @easy_transfer.actual_payer_name, attachment: @easy_transfer.attachment, college_code: @easy_transfer.college_code, hmac_code: @easy_transfer.hmac_code, name_ch: @easy_transfer.name_ch, name_en: @easy_transfer.name_en, payment_types: @easy_transfer.payment_types, promo_code: @easy_transfer.promo_code, serial_number: @easy_transfer.serial_number, source: @easy_transfer.source, student_id_number: @easy_transfer.student_id_number, student_number: @easy_transfer.student_number, submission_time: @easy_transfer.submission_time, tuition: @easy_transfer.tuition, tuition_rmb: @easy_transfer.tuition_rmb }
    end

    assert_redirected_to easy_transfer_path(assigns(:easy_transfer))
  end

  test "should show easy_transfer" do
    get :show, id: @easy_transfer
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @easy_transfer
    assert_response :success
  end

  test "should update easy_transfer" do
    patch :update, id: @easy_transfer, easy_transfer: { actual_payer_email: @easy_transfer.actual_payer_email, actual_payer_mobile: @easy_transfer.actual_payer_mobile, actual_payer_name: @easy_transfer.actual_payer_name, attachment: @easy_transfer.attachment, college_code: @easy_transfer.college_code, hmac_code: @easy_transfer.hmac_code, name_ch: @easy_transfer.name_ch, name_en: @easy_transfer.name_en, payment_types: @easy_transfer.payment_types, promo_code: @easy_transfer.promo_code, serial_number: @easy_transfer.serial_number, source: @easy_transfer.source, student_id_number: @easy_transfer.student_id_number, student_number: @easy_transfer.student_number, submission_time: @easy_transfer.submission_time, tuition: @easy_transfer.tuition, tuition_rmb: @easy_transfer.tuition_rmb }
    assert_redirected_to easy_transfer_path(assigns(:easy_transfer))
  end

  test "should destroy easy_transfer" do
    assert_difference('EasyTransfer.count', -1) do
      delete :destroy, id: @easy_transfer
    end

    assert_redirected_to easy_transfers_path
  end
end
