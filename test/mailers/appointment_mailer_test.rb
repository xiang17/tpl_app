require 'test_helper'

class AppointmentMailerTest < ActionMailer::TestCase
  test "schedule_appointment" do
    mail = AppointmentMailer.schedule_appointment
    assert_equal "Schedule appointment", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

  test "cancel_appointment" do
    mail = AppointmentMailer.cancel_appointment
    assert_equal "Cancel appointment", mail.subject
    assert_equal ["to@example.org"], mail.to
    assert_equal ["from@example.com"], mail.from
    assert_match "Hi", mail.body.encoded
  end

end
