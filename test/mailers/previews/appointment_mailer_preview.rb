# Preview all emails at http://localhost:3000/rails/mailers/appointment_mailer
class AppointmentMailerPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/appointment_mailer/schedule_appointment
  def schedule_appointment
    user = User.first
    AppointmentMailer.schedule_appointment user.appointments.first, 1
  end

  # Preview this email at http://localhost:3000/rails/mailers/appointment_mailer/cancel_appointment
  def cancel_appointment
    user = User.first
    AppointmentMailer.cancel_appointment user.appointments.first, 1
  end

end
