require 'open-uri'

File.open("./Scripts/json.txt", "r") do |f|
  f.each_line do |line|
    #line is always end with \n, so we strip it everytime
    if line.strip.length == 0
	break
    end
    id = line[line.rindex('/')+1,line.rindex('.')-line.rindex('/')-1]
    car = Car.find(id)
    l = open(line.strip)
    s = ''
    l.each_line do |line|
      s = s + line
    end
    #puts s
    car.detail = s
    car.save
  end
end

