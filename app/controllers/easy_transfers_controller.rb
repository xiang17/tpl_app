class EasyTransfersController < InheritedResources::Base
  before_action :authenticate_user!
  before_action :same_user, only: [:show, :edit, :update, :destroy]

  HMAC_FIELDS = [:source, :serial_number, :submission_time, :college_code, :tuition,
                 :tuition_rmb, :payment_types, :name_en, :name_ch, :student_number,
                 :student_id_number, :actual_payer_name, :actual_payer_email, :actual_payer_mobile, :promo_code]

  REQUEST_FIELDS = [:source, :serial_number, :submission_time, :college_code, :tuition,
                    :tuition_rmb, :payment_types, :name_en, :name_ch, :student_number,
                    :student_id_number, :actual_payer_name, :actual_payer_email, :actual_payer_mobile, :promo_code,
                    :hmac_code]

  # GET /easy_transfers/new
  def new
    c = Car.find(params[:car_id])
    @easy_transfer = EasyTransfer.where(user_id: current_user.id, car_id: params[:car_id]).order(:id).last
    if @easy_transfer.nil?
      @easy_transfer = EasyTransfer.new
      @easy_transfer.car = c
      @easy_transfer.tuition = payment_due_usd
      RestClient.log = Rails.logger
      @easy_transfer.tuition_rmb = query_rmb @easy_transfer
    else
      redirect_to @easy_transfer
    end
  end

  # POST /easy_transfers
  # POST /easy_transfers.json
  def create
    @easy_transfer = EasyTransfer.new(easy_transfer_params)
    @easy_transfer.car = Car.find(params[:easy_transfer][:car_id])  #TODO: is the car sale pending or already sold?
    @easy_transfer.user = current_user

    @easy_transfer.tuition = payment_due_usd
    @easy_transfer.tuition_rmb = query_rmb @easy_transfer

    @easy_transfer.payment_types = '其他'
    respond_to do |format|
      if @easy_transfer.save
        format.html { redirect_to @easy_transfer, notice: 'EasyTransfer was successfully created.' }
        format.json { render :show, status: :created, location: @easy_transfer }
      else
        format.html { render :new }
        format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /easy_transfers/1
  # PATCH/PUT /easy_transfers/1.json
  def update
    @easy_transfer = EasyTransfer.find(params[:id])
    RestClient.log = Rails.logger
    respond_to do |format|
      if params[:easy_transfer].blank?
        @easy_transfer.errors.add(:base, 'Step2: Please select a file')
        format.html { redirect_to @easy_transfer, notice: 'Step2: Please select a file' }
        format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
      else
        if !easy_transfer_params.blank?  # Record saved, but not submitted to easytransfer.cn successfully
          if @easy_transfer.update(easy_transfer_params)
            @easy_transfer.update_attribute(:message1, nil)
            format.html { redirect_to @easy_transfer, notice: 'EasyTransfer was successfully updated.' }
            format.json { render :show, status: :ok, location: @easy_transfer }
          else
            format.html { render :show }
            format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
          end
        elsif !easy_transfer_verification_params.blank?  # Request submitted, only need to send verification now
          if @easy_transfer.update(easy_transfer_verification_params)
            @easy_transfer.update_attribute(:message2, nil)
            format.html { redirect_to @easy_transfer, notice: 'EasyTransfer verification was successfully updated.' }
            format.json { render :show, status: :ok, location: @easy_transfer }
          else
            format.html { render :show }
            format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
          end
        end
      end
    end
  end

  # POST /easy_transfers/1/confirm
  # POST /easy_transfers/1/confirm.json
  def confirm
    @easy_transfer = EasyTransfer.find(params[:id])
    RestClient.log = Rails.logger
    if @easy_transfer.result1 != 0
      rjson1 = send_easy_transfer_request
    end
    if @easy_transfer.result1 == 0 && @easy_transfer.result2 != 0
      rjson2 = send_easy_transfer_i20
    end
    respond_to do |format|
      if @easy_transfer.result1 != 0
        @easy_transfer.errors.add(:base, "Step1: Error while requesting easytransfer.cn: #{ rjson1['message'] }.")
        format.html { redirect_to @easy_transfer }
        format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
      elsif @easy_transfer.result2 != 0
        @easy_transfer.errors.add(:base, "Step2: Error while sending verification to easytransfer.cn: #{ rjson2['message'] }.")
        format.html { redirect_to @easy_transfer }
        format.json { render json: @easy_transfer.errors, status: :unprocessable_entity }
      else
        format.html { redirect_to @easy_transfer, notice: 'EasyTransfer was successfully created.' }
        format.json { render :show, status: :created, location: @easy_transfer }
      end
    end
  end

  private

    def easy_transfer_params
      params.require(:easy_transfer).permit(:name_en, :name_ch, :student_number,
                                            :student_id_number, :actual_payer_name, :actual_payer_email,
                                            :actual_payer_mobile, :promo_code, :car_id)
    end

    def easy_transfer_verification_params
      params.require(:easy_transfer).permit(:attachment)
    end

    def calc_request_hmac_code easy_transfer
      message = HMAC_FIELDS.map { |sym| easy_transfer.public_send(sym) }.join('')
      logger.info message
      logger.info Rails.application.secrets.easy_transfer_key
      OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('md5'),
                              Rails.application.secrets.easy_transfer_key,
                              message)
    end

    def calc_verification_hmac_code easy_transfer
      message = 'tripalink' + easy_transfer.serial_number \
                        + easy_transfer.attachment.file.filename
      OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('md5'),
                              Rails.application.secrets.easy_transfer_key,
                              message)
    end

    def calc_dollar_hmac_code easy_transfer
      promo_code = easy_transfer.promo_code ? easy_transfer.promo_code : ''
      message = 'tripalink' + easy_transfer.tuition.to_s + promo_code
      OpenSSL::HMAC.hexdigest(OpenSSL::Digest.new('md5'),
                              Rails.application.secrets.easy_transfer_key,
                              message)
    end

    def send_easy_transfer_request
      timestamp = Time.use_zone("Beijing") do Time.zone.now.strftime("%Y%m%d%H%M%S") end
      @easy_transfer.serial_number = 'TL' + timestamp \
                                    + SecureRandom.random_number(1000000).to_s
      @easy_transfer.submission_time = timestamp

      @easy_transfer.hmac_code = calc_request_hmac_code @easy_transfer
      payload = REQUEST_FIELDS.map { |sym| URI.encode_www_form(sym => @easy_transfer.send(sym)) }.join('&')
      resp = RestClient.post('http://api.easytransfer.cn/order', payload)
      rjson = JSON.parse(resp)
      @easy_transfer.update_attributes({result1: rjson['result'],
                                        message1: rjson['message']})
      @easy_transfer.update_step
      rjson
    end

    def send_easy_transfer_i20
      hmac_code = calc_verification_hmac_code @easy_transfer
      file = File.new(File.join(Rails.root, 'public', @easy_transfer.attachment.url), 'rb')
      resp = RestClient.post('http://api.easytransfer.cn/upload_order_attachment',
                              {:source => 'tripalink',
                               :serial_number => @easy_transfer.serial_number,
                               :attachment => file,
                               :hmac_code => hmac_code})
      rjson = JSON.parse(resp)
      @easy_transfer.update_attributes({result2: rjson['result'],
                                        message2: rjson['message']})
      @easy_transfer.update_step
      rjson
    end

    def exchange_rate
      @exchange_rate ||= Rails.cache.fetch(:exchange_rate, :expires_in => 24.hours) do
        noko = Nokogiri::HTML(open('http://fx.cmbchina.com/hq/').read)
        trs = noko.xpath("//div[@id='realRateInfo']//tr")
        tds = trs[4].xpath('td')
        tds[5].text.strip.to_f
      end
    end

    def payment_due_usd
      car = @easy_transfer.car
      car.price + car.price * 0.09 + car.price_registration_fee + car.price_document_fee + car.price_service_fee - 500
    end

    def query_rmb easy_transfer
      hmac_code = calc_dollar_hmac_code easy_transfer
      data = URI.encode_www_form({:source => 'tripalink',
                                  :dollar => easy_transfer.tuition,
                                  :promo_code => easy_transfer.send(:promo_code),
                                  :hmac_code => hmac_code})
      resp = RestClient.get("http://api.easytransfer.cn/dollar2rmb?#{data}")
      rjson = JSON.parse(resp)
      # @easy_transfer.update_attributes({tuition_rmb: rjson['rmb']})
      rjson['rmb']
    end

    def payment_due_cny(usd)
      cny = (usd + 25) * exchange_rate / 100 * (1 + 0.0028)
      (cny * 100 + 0.5).to_i / 100.0
    end

    def same_user
      unless current_user.id == EasyTransfer.find(params[:id]).user_id
        flash[:error] = 'You can only see your own EasyTransfer request.'
        redirect_to cars_path
      end
    end
end

