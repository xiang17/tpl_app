class ChargesController < ApplicationController
  begin
    require '/root/.rbenv/versions/2.2.3/lib/ruby/gems/2.2.0/gems/stripe-1.27.1/lib/stripe.rb'
  rescue LoadError
    require 'Stripe'
  end
  def charge
    Stripe.api_key = Rails.application.secrets.stripe_sc_live_key
    token = params[:stripeToken]
    begin
      charge = Stripe::Charge.create(
                                 :amount => 87000,
                                 :currency => "usd",
                                 :source => token,
                                 :description => 'email: ' + params[:email].to_s + ' phone: ' + params[:phone].to_s
      )

      #save charge info to database
      c = Charge.new
      c.carid = params[:carid].to_i
      c.price = Car.find(c.carid).price
      c.make = Car.find(c.carid).car_model.make
      c.model= Car.find(c.carid).car_model.model
      c.userid = current_user.id
      #c.userid = User.find_by(email: params[:email]).id
      c.email = params[:email]
      c.phone = params[:phone]
      c.save

      #change this car to sell_pending
      car = Car.find(params[:carid])
      car.status = 4
      car.save
      #send email to both side
      c.send_email

      render :json => {:status => true}
    rescue Stripe::CardError => e
      render :json => {:status => false}
    end
  end

end
