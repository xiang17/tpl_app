json.array!(@easy_transfers) do |easy_transfer|
  json.extract! easy_transfer, :id, :source, :serial_number, :submission_time, :college_code, :tuition, :tuition_rmb, :payment_types, :name_en, :name_ch, :student_number, :student_id_number, :actual_payer_name, :actual_payer_email, :actual_payer_mobile, :hmac_code, :promo_code, :attachment
  json.url easy_transfer_url(easy_transfer, format: :json)
end
