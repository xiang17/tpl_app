class EasyTransfer < ActiveRecord::Base
  belongs_to :car, inverse_of: :easy_transfers
  belongs_to :user, inverse_of: :easy_transfers
  mount_uploader :attachment, AttachmentUploader

  validates_presence_of :tuition
  validates_presence_of :tuition_rmb
  validates_presence_of :name_en
  validates_presence_of :name_ch
  validates_presence_of :student_number
  validates_presence_of :student_id_number
  validates_presence_of :actual_payer_name
  validates_presence_of :actual_payer_email
  validates_presence_of :actual_payer_mobile

  def current_step
    update_step if !@current_step
    @current_step
  end

  def update_step
    @current_step = if new_record?
                      0
                    elsif result1 && result1 != 0  # info error
                      if message1 != nil  # message1 should be set to nil after info is revised
                        1  # prompt to revise info
                      else
                        2  # info revised. prompt to revise i20
                      end
                    elsif result2 && result2 != 0  # i20 error
                      if message2 != nil
                        2  # prompt to revise info
                      else
                        3  # i20 revised. prompt to preview
                      end
                    elsif attachment.blank?
                      2  # first time to file upload
                    elsif result1 == 0 && result2 == 0
                      4  # confirmed
                    else
                      3  # prompt to preview
                    end
  end

end
