require 'jwt'

module AuthToken
  def AuthToken.issue_token(payload, exp=3.months.from_now)
    #payload must have key 'email', which is ensured by authentication check
    payload[:exp] = exp.to_i #default expire set to 3 months
    # payload[:exp] = 1.seconds.from_now.to_i #default expire set to 3 months
    payload[:aud] = Rails.application.secrets.jwt_id
    token = JWT.encode(payload, Rails.application.secrets.jwt_secret_key, 'HS256')
    expert = Expert.find_by(:email => payload[:email])
    expert[:jwt_token] = token unless !expert
    expert.save unless !expert
    token
  end

  def AuthToken.valid?(token)
    begin
      payload = JWT.decode(token, Rails.application.secrets.jwt_secret_key)[0]
      raise InvalidTokenError if Rails.application.secrets.jwt_id != payload["aud"]
      #not expired
      expert = Expert.find_by(:email => payload['email'])
      return payload, (expert[:jwt_token] == token) unless !expert
    rescue JWT::DecodeError, InvalidTokenError, ExpiredSignature
      return payload, false
    end
  end
end
