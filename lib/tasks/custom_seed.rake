# lib/tasks/custom_seed.rake
namespace :db do
  namespace :seed do
    # http://stackoverflow.com/questions/19872271/adding-a-custom-seed-file
    # Add a new rake task:
    # rake db:seed:seed_file_name # Name of the file EXCLUDING the .rb extension
    Dir[File.join(Rails.root, 'db', 'seeds', '*.rb')].each do |filename|
      task_name = File.basename(filename, '.rb').intern    
      task task_name => :environment do
        load(filename) if File.exist?(filename)
      end
    end
  end
end
